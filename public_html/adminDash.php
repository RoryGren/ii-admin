<?php
	session_start();
//	$_SESSION: Array ( 
//	[LoggedIn] => YouAreLoggedInToTheAdminModuleNow 
//	[LogInStart] => 2018-06-05 14:01:22 
//	[LoggedInToken] => New 
//	[UID] => Array ( 
//		[0] => 1 
//		[Per_Id] => 1 
//		[1] => Rory 
//		[Per_Name] => Rory ) )
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
			include_once "config.php";
			include_once ROOT     . "/includes/adminConfig.php";
			include_once WEB_ROOT . '/classes/classLogin.php';
?>
<!DOCTYPE html>
<?php 
	include WEB_ROOT . '/includes/head.php'; 
?>
<body>
	<script>
		$(document).ready(function() {
				$('#navHome').siblings('li').removeClass('active');
				$('#navHome').addClass('active');
				sessionStorage.setItem('module', 'home');
			});
	</script>
	<?php
		include WEB_ROOT . '/includes/navBarAdmin.php';
	?>
	<h3>Admin Dash...</h3>
	<div class="container-fluid">
		<div class="row">
			<div class="col col-lg-1"></div>
			<div class="col col-lg-10" id="working">
				<?php	include 'ii-CRM/crmWebContact.php'; ?>
			</div>
			<div class="col col-lg-1"></div>
		</div>
	</div>
    </body>
</html>
<?php 
	}
	else {
	header('location: index.php');
	}
?>