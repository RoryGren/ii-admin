<?php
	session_start();
//	$_SESSION: Array ( 
//	[LoggedIn] => YouAreLoggedInToTheAdminModuleNow 
//	[LogInStart] => 2018-06-05 14:01:22 
//	[LoggedInToken] => New 
//	[UID] => Array ( 
//		[0] => 1 
//		[Per_Id] => 1 
//		[1] => Rory 
//		[Per_Name] => Rory ) )
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
			include_once "config.php";
			include_once ROOT     . "/includes/adminConfig.php";
			include_once WEB_ROOT . '/classes/classLogin.php';
?>
<!DOCTYPE html>
<?php 
	include WEB_ROOT . '/includes/head.php'; 
?>
	<body>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-lg-12" id="working">
					<h3>Admin Support...</h3>
					<?php
						include WEB_ROOT . '/ii-Admin/adminSupport.php';
					?>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function() {
					$('#navHome').siblings('li').removeClass('active');
					$('#navHome').addClass('active');
					sessionStorage.setItem('module', 'home');
				});
		</script>
    </body>
</html>
<?php 
	}
	else {
	header('location: index.php');
	}
