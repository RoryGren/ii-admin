<?php
include dirname(__FILE__) . '/classStudent.php';
include_once dirname(__FILE__) . '/traitDisplay.php';
/**
 * Description of classStudentDisplay
 *
 * @author rory
 */
class classStudentDisplay extends classStudent {
	use traitDisplay;
	
	public function __construct($UserId) {
		parent::__construct($UserId);
	}
	
	public function getStudentList() {
		return $this->returnStudentList();
	}
	
	public function getPersonList() { 
		return $this->returnPersonList();
	}
	
	public function returnUserIdNo($IDNo) {
		return $this->checkUserId($IDNo);
	}
		
	public function tableHeader($ArrayData) {
		$Headers = array_keys($ArrayData);
		$TableHeader = "<table id='tableList' class='table table-hover table-striped table-bordered table-responsive table-list text-small'><thead><tr class='filters'>";
		foreach ($Headers as $Key => $Field) {
			if (!strpos($Field, 'Id') && !is_numeric($Field) && $Field !== 'isActive' && $Field !== 'User_Nick' && $Field !== 'Branch_Desc' && $Field !== 'Att_Cert') { // @id => do not want to display record id
				$TableHeader .= "<th><input type='text' class='form-control text-center' placeholder='";
				$TableHeader .= str_replace('_', ' ', $Field);
				$TableHeader .= "' disabled></th>";
			}
		}
		$TableHeader .= "</tr></thead>";
		echo $TableHeader;
	}

	public function tableBody($ArrayData) {
//		print_r($ArrayData);
		$TableBody = "<tbody>";
		foreach ($ArrayData as $Key => $Row) {
//			$TableBody .= "<tr type='button' id='$Key' onclick='// showDetail($Key)'>"; // data-toggle='modal' data-target='#modalDetail'>";
			$TableBody .= "<tr type='button' id='$Key'>";
			foreach ($Row as $Field => $Value) {
				if  (!strpos($Field, 'Id') && !is_numeric($Field) && $Field !== 'isActive' && $Field !== 'User_Nick' && $Field !== 'Branch_Desc' && $Field !== 'Att_Cert') {
					$TableBody .= "<td>$Value</td>";
				}
			}
			$TableBody .= "</tr>";
		}
		$TableBody .= "</tbody></table>";
		echo $TableBody;
	}
	
	public function showStudentDetail($rowId, $selectedCourseId) {
		// ===== showStudentDetail($studentId) =====
//		echo "User rowId: $rowId<br>";
		$studentCourseList = $this->fetchStudentCourseList($rowId);
		if (is_null($selectedCourseId) || $selectedCourseId === '') {
			$selectedCourseId = 'zzzz';
		}
//		echo "User courseId: $selectedCourseId<br>";
		$formHead     = "<form id='frmStudentDetail'>";
		$formFoot     = "</form>";
		$Hidden       = "<input type='hidden' id='Student_Id' value='$rowId'>";
		$Hidden		 .= "<input type='hidden' id='Selected_Course_Id' value='$selectedCourseId'>";
		$tableHead    = "<table class='table table-responsive table-borderless'><tbody>";
		$tableBody    = "";
		$tableCourses = "";
		$tableFoot    = "</tbody></table>";
		$rowClass     = "class='disabled'";
		$checkButton  = "";
		if ($rowId !== "new" && $rowId !== '') {
			$UserList = $this->returnUserList(); 
			$StudentDetail = $UserList[$rowId];
			$rowClass = "";
			$checkButton = "class='hidden'";
		}
// =================================================================================
// ===== List of Courses -> Current course being added to selected with jQuery =====
// =================================================================================

		$tableBody .= "<tr $rowClass><td><label for='Course_Id'>Courses:</label></td>"
			. "<td>"
			. $this->getSelect('Course_Id', $this->CourseList, $selectedCourseId, 1)
			. "</td>"
			. "</tr>";

// ===== ID number =====

		$tableBody .= "<tr><td><label for='User_Id_Number'>ID Number:</label></td>       <td><input type='text' class='form-control' id='User_Id_Number'               required value='" . $StudentDetail['User_Id_Number'] . "' autoselect></td></tr>";

// =================================================================================
// ===== CheckId Button - only display for "New" students - hidden with jQuery =====
// =================================================================================

		$tableBody .= "<tr $checkButton><td colspan='2' class='text-center'><button class='btn btn-primary' id='checkIdButton' onclick=\"checkId($('#User_Id_Number').val());\">Check ID</button></td></tr>";

// ========================================================================
// ===== Rest of form - only display when studentId has been found... =====
// ========================================================================

		$tableBody .= "<tr $rowClass><td><label for='User_Last'>Surname:</label></td>    <td><input type='text' class='form-control' id='User_Last' name='User_Last'   required value='" . $StudentDetail['User_Last'] . "' onKeyUp='doCert();'></td></tr>";
		$tableBody .= "<tr $rowClass><td><label for='User_First'>First Name:</label></td><td><input type='text' class='form-control' id='User_First' name='User_First' required value='" . $StudentDetail['User_First']   . "' onKeyUp='doCert();'></td></tr>";
		$tableBody .= "<tr $rowClass><td><label for='User_Nick'>Known As:</label></td>   <td><input type='text' class='form-control' id='User_Nick' name='User_Nick'            value='" . $StudentDetail['User_Nick']   . "'></td></tr>";
		$tableBody .= "<tr $rowClass><td><label for='User_Co_Id'>Company:</label></td>   <td>"
						. $this->getSelect('User_Co_Id', $this->CoyList, $StudentDetail['User_Co_Id'], 2)
						. "</td></tr>";
//		if ($rowId === "new" && $rowId === '') {
		$tableBody .= "<tr $rowClass><td><label for='Att_Cert'>Name on Certificate:</label></td><td><input type='text' class='form-control' id='Att_Cert' name='Att_Cert' required value='" . $StudentDetail['Att_Cert']   . "'></td></tr>";

		$tableBody .= "<tr $rowClass><td><label for='User_Email'>Email:</label></td>     <td><input type='text' class='form-control' id='User_Email' name='User_Email'    required value='" . $StudentDetail['User_Email']   . "'></td></tr>";
		$tableBody .= "<tr $rowClass><td><label for='User_Tel_No'>Tel:</label></td>      <td><input type='text' class='form-control' id='User_Tel' name='User_Tel'           value='" . $StudentDetail['User_Tel']   . "'></td></tr>";
		$tableBody .= "<tr $rowClass><td><label for='User_Cel_No'>Mobile:</label></td>   <td><input type='text' class='form-control' id='User_Cel' name='User_Cel'  required value='" . $StudentDetail['User_Cel']   . "'></td></tr>";

		if (count($studentCourseList) > 0) {
//			[Course_Number] => 1524 [App_Desc] => PowerOffice Electrification [Course_Start_Date] => 2015-06-30 [Venue_Short] => Brackenfell [Trainer_Code] => RKG ) 
			$tableCourses .= "<tr $rowClass><td colspan='2'><label>Previous Courses:</label></td></tr>";
			foreach ($studentCourseList as $Course => $data) {
				$tableCourses .= "<tr $rowClass>";
				$tableCourses .= "<td>" . $data['Course_Start_Date'] . "</td>";
				$tableCourses .= "<td>" . $data['App_Desc'] . "</td>";
				$tableCourses .= "<td>" . $data['Venue_Short'] . "</td>";
				$tableCourses .= "<td>" . $data['Trainer_Code'] . "</td>";
				$tableCourses .= "<td>" . $data['Trainer_Code'] . "</td>";
				$tableCourses .= "</tr>";
			}
		}

		echo $formHead . $Hidden . $tableHead . $tableBody . $tableFoot . $tableHead . $tableCourses . $tableFoot . $formFoot;
	}

	public function updateStudent($id, $info, $UserId, $job, $sender) { 
	/*
	 *	$id => id of record to be updated, 'New' or record id
	 *	$info => data from form, 
	 *	$UserId => for logging, 
	 *	$job => update, insert, delete, 
	 *	$sender => Student, Course :: Set table to update
	 */
	echo "classStudentDisplay.php :: updateStudent()<br>";
	$this->updateStudentRecord($id, $info, $UserId, $job, $sender);
	}
	
	public function getNewStudentId() {
		echo "<form class='form-inline'>";
		echo "<div class='form-group'>";
		echo "<label for='User_Id_Number'>ID Number:&nbsp;&nbsp;<input type='text' class='form-control' id='User_Id_Number' required autoselect>&nbsp;&nbsp;";		
		echo "<button type='button' class='btn btn-primary' id='checkIdButton' onclick=\"checkId($('#User_Id_Number').val());\">Check ID</button>";
		echo "</div></form>";
	}
	
	public function userEnrolledInCourse($userRowId, $courseRowId) {
		if ($this->userNotInCourse($userRowId, $courseRowId)) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	public function classList($courseId) {
//		echo "<br>classList($courseId)<br>";
		$counter = 1;
		$nick = '';
		$tableTop = "<table class='table table-bordered'>";
		$tableTop .= "<tr><td colspan='2'>&nbsp;</td><td class='text-center notice'>Remove</td></tr>";
		$table    = "";
		$tableEnd = "</table>";
		$classList = $this->returnCourseStudentList($courseId);
		foreach ($classList as $key => $data) {
			if (is_null($data['User_Nick'])) {$nick = $data['User_First'];}
			else {$nick = $data['User_Nick'];}
			$table .= "<tr><td>$counter</td><td>" . $data['User_Last'] . ", " . $data['User_First'] . " ($nick)</td>";
			$table .= "<td class='text-center'>"
					. "<input type='checkbox' class='removeStudent' id='$courseId' value='" . $data['User_Id_Key'] . "'>"
					. "</td>";
			$table .= "</tr>";
			$counter++;
		}
		echo $tableTop . $table . $tableEnd;
	}
}

?>
