<?php
include_once dirname(__FILE__) . '/traitCRUD.php';
/**
 * Description of classAdmin
 * 
 * Adding and editing back end users
 *
 * @author rory
 */
class classAdmin extends mysqli {
	use traitCRUD;

	private $userList;
	private $userId;

	public function __construct($UserId) {
		$this->userId = $UserId;
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->fetchUserList();
	}
	
	protected function returnUserList() {
		return $this->userList;
	}
	
	private function fetchUserList() {
		$sql = "SELECT `Per_Id`, `Per_Desc`, `Per_Name`, `Per_Short`, `Per_Level` 
				FROM `Permissions`
				ORDER BY `Per_Level` DESC,`Per_Name`";
		$result = $this->query($sql);
		while ($row =  mysqli_fetch_assoc($result)) {
			$this->userList[$row['Per_Id']] = $row;
		}
		mysqli_free_result($result);
	}

	protected function updateUser($rowId, $info) {
		$sql = '';
		$data = json_decode($info);
		if ($rowId === 'new' && $data->task === 'btnSave') {
			$sql = "Insert into `Permissions` (`Per_Desc`, `Per_Code`, `Per_Name`, `Per_Short`, `Per_Level`, `CreateUser`, `CreateDate`, `ModUser`, `ModDate`) ";
			$sql .= "values('" 
					. $data->Per_Desc 
					. "', PASSWORD('NewPassword123*'), '" 
					. $data->Per_Name . "', '" 
					. $data->Per_Short . "', '" 
					. $data->Per_Level . "', " 
					. $this->userId[0] . ", '"
					. TODAY . "', "
					. $this->userId[0] . ", '"
					. TODAY . "')";
		}
		else {
			$sql = "UPDATE `Permissions` "
					. "SET ";
			$Fields = '';
			$sqlWhere = "WHERE `Per_Id` = $rowId";
			switch ($data->task) {
				case 'btnSave':
					$Fields .= "`Per_Desc` = '" . $data->Per_Desc . "', ";
					$Fields .= "`Per_Name` = '" . $data->Per_Name . "', ";
					$Fields .= "`Per_Short` = '" . $data->Per_Short . "', ";
					$Fields .= "`Per_Level` = '" . $data->Per_Level . "'";
					break;
				case 'btnReset':
					$Fields = "`Per_Code` = PASSWORD('" . $data->Pwd . "')";
					break;
				case 'btnDisable':
					$Fields = "`Per_Level` = '0' ";
					break;
				case 'btnReactivate':
					$Fields = "`Per_Level` = '1111'";
					break;
				default:
					break;
			}
			$Fields .= ", `ModUser` = " . $this->userId[0] . ", `ModDate` = '" . TODAY . "' ";
			$sql .= $Fields . $sqlWhere;
		}
		echo "$sql<br>";
		if ($this->query($sql)) {echo "Success";}
	}
	
	protected function fetchUnlocks($searchText) {
		
	}

}

?>
