<?php

include dirname(__FILE__) . '/classCourse.php';
include_once dirname(__FILE__) . '/traitDisplay.php';

/**
 * Description of classCourseDisplay
 *	Controls display aspect of course data 
 *	retrieved from classCourse
 * @author rory
 */
class classCourseDisplay extends classCourse {
	use traitDisplay;
	
	public function __construct($UserId) {
		parent::__construct($UserId);
	}
	
	public function getCourseList() {
		return $this->CourseList;
	}
	
	public function returnMasters($Type) {
		return $this->{$Type};
	}
	
	public function newCourseNumber($startDate) {
//		echo "$startDate: Yes! ";
//		$this->getNextCourseNumber($startDate);
		echo $this->getNextCourseNumber($startDate);
	}
	
	public function updateCourse($courseId, $info, $userId, $job) {
//		echo "<br>==========================================";
//		echo "<br>===== classCourseDisplay=>updateCourse() ======";
//		echo "<br>==========================================<br>";
//		echo "UserId: " . $userId . " ::: " . $this->UserId . "<br>";
//		echo "CourseId: " . $courseId . "<br>";

		$ret = FALSE;
		if ($userId == $this->UserId && $this->doUpdateCourse($courseId, $info, $job)) {
			$ret = TRUE;
		}
		return $ret;
	}
		
	public function tableHeader($ArrayData) {
		$Headers = array_keys($ArrayData);
		$TableHeader = "<table id='tableList' class='table table-hover table-striped table-bordered table-responsive table-list text-small text-center'><thead><tr class='filters'>";
		foreach ($Headers as $Key => $Field) {
			if (!strpos($Field, 'Id')) { // @id => do not want to display record id
				$TableHeader .= "<th><input type='text' class='form-control text-center' placeholder='";
				$TableHeader .= str_replace('_', ' ', $Field);
				$TableHeader .= "' disabled></th>";
			}
		}
		$TableHeader .= "</tr></thead>";
		echo $TableHeader;
	}

	public function tableBody($ArrayData) {
		$TableBody = "<tbody>";
		foreach ($ArrayData as $Key => $Row) {
//			$TableBody .= "<tr type='button' id='$Key' onclick='// showDetail($Key)'>"; // data-toggle='modal' data-target='#modalDetail'>";
			$TableBody .= "<tr type='button' id='$Key'>";
			foreach ($Row as $Field => $Value) {
				if  (!strpos($Field, 'Id')) {
					$TableBody .= "<td>$Value</td>";
				}
			}
			$TableBody .= "</tr>";
		}
		$TableBody .= "</tbody></table>";
		echo $TableBody;
	}
	
	public function courseDetail($courseId) {
//		echo "<br>courseDetail($courseId)<br>";
//		Array ( [Course_Id] => 281 [Course_Number] => 1717 [Course_Start_Date] => 2017-11-14 [Course_End_Date] => 2017-11-16 [App_Id] => 20 [Trainer_Id] => 0 [Venue_Id] => 99 [Course_Status] => 1 )
		if (!isset($courseId)) {$courseId = 'New';}
//		print_r($courseClassList);
//		echo "<br>";
		$formHead  = "<form id='frmCourseDetail'>";
		$formFoot  = "</form>";
		$tableHead = "<table class='table table-responsive table-borderless'><tbody>";
		$tableFoot = "</tbody></table>";
		$CourseDetail = $this->getCourseDetail($courseId);
		$learnerCount = $this->hasLearners($courseId);
		if ($courseId === 'New') {
//			$CourseNumber = $this->getNextCourseNumber();
			$CourseNumber = "New";
		}
		else {
			$CourseNumber = $CourseDetail['Course_Number'];
		}
		$Hidden     = "<input type='hidden' id='Course_Id' value='" . $courseId . "'>";
		$tableBody  = "<tr><td><label for='Course_Number'>Course Number:</label></td>        <td><input type='text' class='form-control' id='Course_Number'     required value='" . $CourseNumber . "'></td></tr>";
		$tableBody .= "<tr><td><label for='Course_Start_Date'>Course Start Date:</label></td><td><input type='date' class='form-control' id='Course_Start_Date' required value='" . $CourseDetail['Course_Start_Date'] . "'></td></tr>";
		$tableBody .= "<tr><td><label for='Course_End_Date'>Course End Date:</label></td>    <td><input type='date' class='form-control' id='Course_End_Date'   required value='" . $CourseDetail['Course_End_Date']   . "'></td></tr>";
		$tableBody .= "<tr><td><label for='App_Id'>Course:</label></td>						 <td>"
				. $this->getSelect('App_Id', $this->AppList, $CourseDetail['App_Id'], 2)
				. "</td></tr>";
		$tableBody .= "<tr><td><label for='Trainer_Id'>Trainer:</label> </td>				 <td>"
				. $this->getSelect('Trainer_Id', $this->TrainerList, $CourseDetail['Trainer_Id'], 1)
				. "</td></tr>";
		$tableBody .= "<tr><td><label for='Venue_Id'>Venue:</label></td>					 <td>"
				. $this->getSelect('Venue_Id', $this->VenueList, $CourseDetail['Venue_Id'], 2)
				. "</td></tr>";
		$tableBody .= "<tr><td><label for='Course_Status'>Status:</label></td>				 <td>"
				. $this->getSelect('Course_Status', $this->StatusList, $CourseDetail['Course_Status'], 0)
				. "</td></tr>";
		$tableBody .= "<tr><td>Students:</td><td><span id='learnerCount'>$learnerCount</span>";
		if ($learnerCount > 0) {
			$tableBody .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='listExistingStudents();'>Show Class List</a>";
		}
		$tableBody .= "</td></tr>";
		echo $formHead . $Hidden . $tableHead . $tableBody . $tableFoot . $formFoot;
	}
	
	private function getSelect_Old($selId, $listArray, $selectedItemId, $valuePosition) { // valuePosition = array key
		// transferred to traitDisplay...
//		$selTop = "<select id='$selId' class='form-control' onchange='doChange(this.id, this.value);'>";
//		$selFooter = "</select>";
//		$selBody = "<option value='zzzz'";
//		if (is_null($selectedItemId)) {
//			$selBody .= " SELECTED";
//		}
//		$selBody .= ">==== Select ====</option>";
//		if ($selId !== 'Course_Status') {
//			foreach ($listArray as $key => $value) {
//				$ListId = $value[0];
//				$selBody .= "<option value='" . $value[0] . "'";
//				if ($selectedItemId === $value[0]) {
//					$selBody .= " SELECTED";
//				}
//				$selBody .= ">";
//				$selBody .= $value[$valuePosition];
//				$selBody .= "</option>";
//			}
//		}
//		else {
//			foreach ($listArray as $key => $value) {
//				$selBody .= "<option value='$key'";
//				if ($selectedItemId == $key) {
//					$selBody .= " SELECTED";
//				}
//				$selBody .= ">$value</option>";
//			}
//		}
//		if ($selId !== "Course_Status" && $selId !== 'App_Id') {
//			$selBody .= "<option value='createNew'> === Create New === </option>";
//		}
//		return $selTop . $selBody . $selFooter;
	}
	
	public function clash($ClashType, $ClashID, $ClashStart, $ClashEnd) {
		return $this->findClash($ClashType, $ClashID, $ClashStart, $ClashEnd);
	}
	
}

?>
