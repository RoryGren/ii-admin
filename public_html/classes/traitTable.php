<?php

trait traitTable {
		
	public function tableHeader($ArrayData) {
		
		$Headers = array_keys($ArrayData[key($ArrayData)]); // Array ( [Id] => 123 [Code] => [Description] => Redwing Technologies )  

		$TableHeader = "<table id='tableList' class='table table-hover table-striped table-bordered table-responsive table-list text-small'>"
				. "<thead><tr class='filters'>";
		foreach ($Headers as $Key => $Field) {
			if (!strpos($Field, 'Id') && !is_numeric($Field) && $Field !== 'Id' && $Field !== 'isActive' && $Field !== 'Is_Active' && $Field !== 'User_Nick' && $Field !== 'Branch_Desc' && $Field !== 'Att_Cert') { // @id => do not want to display record id
				$TableHeader .= "<th><input type='text' class='form-control text-center' placeholder='";
				$TableHeader .= str_replace('_', ' ', $Field);
//				$TableHeader .= $Field;
				$TableHeader .= "' disabled></th>";
			}
		}
		$TableHeader .= "</tr></thead>";
		echo $TableHeader;
	}

	public function tableBody($ArrayData) {
		$TableBody = "<tbody>";
		foreach ($ArrayData as $Key => $Row) {
			$TableBody .= "<tr type='button' id='$Key'";
			$TableBody .= ">";
			foreach ($Row as $Field => $Value) {
				if  (!strpos($Field, 'Id') && !is_numeric($Field) && $Field !== 'Id' && $Field !== 'isActive' && $Field !== 'Is_Active' && $Field !== 'User_Nick' && $Field !== 'Branch_Desc' && $Field !== 'Att_Cert') {
					$TableBody .= "<td>$Value</td>";
				}
			}
			$TableBody .= "</tr>";
		}
		$TableBody .= "</tbody></table>";
		echo $TableBody;
	}
		
}
?>
