<?php
include dirname(__FILE__) . '/classPerson.php';
include_once dirname(__FILE__) . '/traitDisplay.php';
include_once dirname(__FILE__) . '/traitTable.php';

/**
 * Description of classContactDisplay
 *
 * @author rory
 */
class classContactDisplay extends classPerson {
	use traitTable;
	use traitDisplay;

	public function __construct($UserId) {
		parent::__construct($UserId);
		$this->UserId = $UserId;

	}

	public function getContactList() {
		return $this->userList();
	}
	
}

?>
