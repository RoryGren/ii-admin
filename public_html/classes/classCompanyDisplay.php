<?php
include dirname(__FILE__) . '/classCompany.php';

/**
 * Description of classCompanyDisplay
 *
 * @author rory
 */
class classCompanyDisplay extends classCompany {

	public function __construct($UserId) {
		parent::__construct($UserId);
	}
	
	public function companyDetail($rowId) {
		$company = $this->CompanyList[$rowId];
//		Array ( [Id] => 362 [Code] => [Description] => AngloAmerican [IsActive] => 1 )
		return $company;
	}
	
	public function branchDetail($branchId) {
		echo "<div class='container-fluid noPadding'>";
			echo "<div class='row'>";
				echo "<div class='col col-sm-6 formContainer'>";
					echo "<form id='frmCompanyDetails'>";
					echo $this->showBranchDetails($branchId);
					echo "</form>";
				echo "</div>";
				echo "<div class='col col-sm-8 googleContainer'>";
				echo "</div>";
			echo "</div>";
		echo "</div>";
	}
	
	public function showBranchDetails($branchId) {
		$branchData = array();
		$branch = $this->fetchBranch($branchId);
		$branchData = $this->fetchBranchDetail($branchId);
		$arrayLength = count($branchData);
//		Array ( [752] => Array ( [0] => 752 [AttribValId] => 752 [1] => 3 [AttribId] => 3 [2] => 273 [ParentId] => 273 [3] => Vryheid [AttribVal] => Vryheid [4] => Area [AttribDesc] => Area [5] => 1 [AttribDisplayOrder] => 1 [6] => 1 [isMandatory] => 1 [7] => 1 [AttribStatus] => 1 ) [] => Array ( [0] => [AttribValId] => [1] => [AttribId] => [2] => [ParentId] => [3] => [AttribVal] => [4] => Physical Address [AttribDesc] => Physical Address [5] => 7 [AttribDisplayOrder] => 7 [6] => 1 [isMandatory] => 1 [7] => 1 [AttribStatus] => 1 ) [437] => Array ( [0] => 437 [AttribValId] => 437 [1] => 2 [AttribId] => 2 [2] => 273 [ParentId] => 273 [3] => PO Box 57,<>Vryheid,<>3100<> [AttribVal] => PO Box 57,<>Vryheid,<>3100<> [4] => Postal Address [AttribDesc] => Postal Address [5] => 6 [AttribDisplayOrder] => 6 [6] => 1 [isMandatory] => 1 [7] => 1 [AttribStatus] => 1 ) [3118] => Array ( [0] => 3118 [AttribValId] => 3118 [1] => 4 [AttribId] => 4 [2] => 273 [ParentId] => 273 [3] => South Africa [AttribVal] => South Africa [4] => Country [AttribDesc] => Country [5] => 8 [AttribDisplayOrder] => 8 [6] => 1 [isMandatory] => 1 [7] => 1 [AttribStatus] => 1 ) ) 

		$attribTableBody = '';
//		====== Branch ==========================================================
//		====== Identified by data-branchid :: field identified by id ===========
		$attribTableBody .= "<div class='form-group'>";
		$attribTableBody .= "<label for='Branch_Code' class='control-label'>Branch Code</label>";
		$attribTableBody .= "<input type='text' data-branchid='$branchId' class='form-control branchData' id='Branch_Code' data-oldvalue='" . $branch['Branch_Code'] . "' value='" . $branch['Branch_Code'] . "'>";
		$attribTableBody .= "</div>";
		$attribTableBody .= "<div class='form-group'>";
		$attribTableBody .= "<label for='Branch_Desc' class='control-label'>Branch</label>";
		$attribTableBody .= "<input type='text' data-branchid='$branchId' class='form-control branchData' id='Branch_Desc' data-oldvalue='" . $branch['Branch_Desc'] . "' value='" . $branch['Branch_Desc'] . "'>";
		$attribTableBody .= "</div>";
//		====== Branch Attrib Vals ==============================================
//		====== Identified by data-branchid :: field identified by id ===========
		foreach ($branchData as $key => $attribVal) {
			$attribTableBody .= "<div class='form-group'>";
			$attribTableBody .= "<label for='" . $attribVal['AttribValId'] . "'>" . $attribVal['AttribDesc'] . "</label>";
			if (strpos(strtoupper($attribVal['AttribDesc']), 'ADDRESS') > 0) {
				$attribTableBody .= "<textarea class='form-control address-box branchData' data-oldvalue='" . $attribVal['AttribVal'] . "' id='" . $attribVal['AttribValId'] . "'>" . $attribVal['AttribVal'] . "</textarea>";
			}
			else {
				$attribTableBody .= "<input type='text' class='form-control branchData' "
						. "id='" . $attribVal['AttribValId'] . "' "
						. "data-attribid='" . $attribVal['AttribId'] . "' "
						. "data-oldvalue='" . $attribVal['AttribVal'] . "' "
						. "data-parentid='$key' "
						. "value='" . $attribVal['AttribVal'] . "'>";
			}
			$attribTableBody .= "</div>";
		}
		return $attribTableBody;
//		echo "</table>";
//		echo "</div>";
//		echo "<br>========================================================<br>";
//		echo "Branch Details:<br>";
//		print_r($this->fetchBranchDetail($branchId));
//		echo "<br>========================================================<br>";
	}
	
	public function getCompanyList() {
		return $this->CompanyList;
	}
	public function companyList() {
		$ArrayData = $this->CompanyList;
//		$Headers = array_keys($ArrayData[key($ArrayData)]); // [205] => Array ( [Id] => 205 [Code] => YBG [Description] => YBG Consulting [IsActive] => 1 ) 

		$TableHeader = "<table id='tableList' class='table table-hover table-striped table-bordered table-responsive table-list text-small'>"
				. "<thead><tr class='filters'>";
		$TableHeader .= "<th>Search:</th><th><input type='text' class='form-control text-center' placeholder='Company'></th></tr></thead>";
		echo $TableHeader;
		$TableBody = "<tbody>";
		foreach ($ArrayData as $Key => $Row) {
			$TableBody .= "<tr type='button' data-rowid='$Key'";
			if ($Row['IsActive'] !== '1') {
				$TableBody .= " class='inactive'";
			}
			if (!$Row['Branch_Code']) {
				$BranchAbbr = $Row['Branch'];
			} else {
				$BranchAbbr = $Row['Branch_Code'];
			}
			$TableBody .= ">";
			$TableBody .= "<td>" . $Row['Code'] . "</td>";
			$TableBody .= "<td>" . $Row['Company'] . " ($BranchAbbr)</td>";
			$TableBody .= "</tr>";
		}
		$TableBody .= "</tbody></table>";
		echo $TableBody;
	}

}

?>
