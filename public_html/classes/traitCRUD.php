<?php
trait traitCRUD {
	
	protected function insertRecord($sql) { //exec SQL and return new rowId
		$query = $this->query($sql);
		return $this->insert_id;
	}
	
	protected function updateRecord($sql) { //exec SQL
		$this->query($sql);
	}

	protected function fetchList($sql) { // Returns full array of objects with id (first element of select) as key
		$List = array();
		$result = $this->query($sql);
		while ($row = mysqli_fetch_array($result)) {
			$List[$row[0]] = $row;
		}
		mysqli_free_result($result);
		return $List;
	}

	protected function fetchListAssoc($sql) { 
/*
 * returns full assoc array of objects
 */
//		echo "<br>fetchListAssoc()<br>";
		$List = [];
		$result = $this->query($sql);
		while ($row = mysqli_fetch_assoc($result)) {
			$List[$row['Id']] = $row;
		}
		mysqli_free_result($result);
		return $List;
	}
	
	protected function fetchTupleArray($sql) { 
/** 
 *	returns tuple array :: key => value
 */ 
		$result = $this->query($sql);
		while ($row = mysqli_fetch_array($result)) {
			$List[$row[0]] = $row[1];
		}
		mysqli_free_result($result);
		return $List;
	}
	
}

?>
