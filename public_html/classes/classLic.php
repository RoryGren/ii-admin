<?php

/**
 * Description of classLic
 *
 * @author rory
 */
class classLic extends mysqli {

	private $SearchString;
	private $isSerialNo = false;
	private $ServerString;
	private $StringCount;
	private $AppCode;
	private $UnlockModeList;
	private $UnlockMode;
	private $UnlockModeId;
	
	public  $AppDesc;
	public  $AppAbbreviation;
	public  $ServerDongle;
	public  $Status;
	public  $LastDate;
	public  $DongleUser;
	public  $AppLimits;
	private $CoyList;
	public  $CompanyId;
	public  $Company;
	public  $BranchId;
	public  $Branch;
	public  $Requester;
	public  $Email;

	public function __construct($UserId, $SearchString) {
		$this->userId = $UserId;
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->SearchString  = $SearchString;
		if (substr_count($SearchString, "-") === 9) {
			$this->isSerialNo = true;
			$this->decodeSerial();
		}
//		self::getUnlockModeList();
//		self::getCoyList();
//		self::decodeSearchString();
	}
	
	private function decodeSearchString() {
		
	}

	private function decodeSerial() {
		$this->ServerString = substr($this->SearchString, 0, 8);
		if ($this->checkSerialComplete()) {										// ========== Check to see if this is a valid serial number     ==========
			echo "Serial Number structure is valid.<br>";
			if ($this->checkOldSerial()) {										// ========== Check to see if this is an unused serial number   ==========
				echo "Serial Number has not been used before.<br>";
				if ($this->checkFirstTimeUse()) {								// ========== Check for Server / Dongle first time use		    ==========
					echo "This is a first time unlock.<br>";					// TODO ========== Register Server / Dongle in Server table		    ==========
					
				}
				else {															// ========== Decode Serial Number for correct company / Dongle ==========
					echo "Server String exists.<br>";
					if ($this->StringCount == 1) {								// TODO ========== Decode and get License Mode and Company			==========
						$this->UnlockMode = substr($this->Serial, 39, 4);
						echo "Mode: " . $this->UnlockModeList[$this->UnlockMode]['Desc'] . "<br>";
						$this->UnlockModeId = $this->UnlockModeList[$this->UnlockMode]['Id'];
						echo "Unlock Mode Id: " . $this->UnlockModeId . "<br>";
						$this->AppCode    = substr($this->Serial, 44, 4);
						$this->getApp();
						echo "Application: " . $this->AppDesc . "<br>";
						$this->getDetails();
						if ($this->CompanyId > 0) { 
							echo "Server / Dongle: " . $this->ServerDongle . "<br>";
							echo "CompanyId: " . $this->CompanyId . "<br>";
							$this->Company = $this->CoyList[$this->CompanyId];
							echo "Company: " . $this->Company['Desc'] . "<br>";
						}
						else {
//							if ($this->ServerDongle > 0) {
								echo "This Server / Dongle has not been used for " . $this->UnlockModeList[$this->UnlockMode]['Desc'] . " before.<br>";
//							}
						}
					}
					else {														// TODO ========== Check which instance is correct and then decode   ========== 
						echo "More than one instance of this server string $this->ServerString.<br>";
						// TODO ========== Get all records with same ServerString and ModeId
					}
				}
			}
			else {
				echo "<span style='color: #DD0000; font-weight: bold;'>Serial Number Rejected: Used previously.</span><br>";
				echo "Server/Dongle: " . $this->ServerDongle . "<br>";
				echo "Requested by " . $this->Requester . " on " . $this->LastDate . "<br>";
			}
		}
		else {
			echo "<span style='color: #DD0000; font-weight: bold;'>Serial Number Rejected: Invalid format</span><br>";
		}
		echo $this->LastDate . "<br>" . $this->DongleUser . "<br>" . $this->AppLimits . "<br>" . $this->Requester . "<br>" . $this->Email;

	}
	
	private function getServerName() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT count(*) FROM `SDServer` WHERE `Status` = 1 AND `ServerString` = ?");
		$query->bind_param("s", $this->ServerString);
		$query->bind_result($NumRecs);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
	}


	private function checkSerialComplete() {
		$Serial_Dots   = substr_count($this->SearchString,'.');
		$Serial_Dashes = substr_count($this->SearchString,'-');
		$ValidSString  = substr_count($this->ServerString,'-');
//		echo "ServerString: " . $this->ServerString . "<br>";
//		echo "SerialDots: $Serial_Dots :: SerialDashes: $Serial_Dashes :: ValidServerString: $ValidSString<br>";
		if (($Serial_Dots === 2) && ($Serial_Dashes === 9) && ($ValidSString === 0)) {
			return true;
		}
		else {
			return false;
		}
	}

	private function checkOldSerial() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT count(*), `ExpiryDate`, `Requester` FROM `SDUnlock` WHERE `Serial` = ?");
		$query->bind_param("s", $this->Serial);
		$query->bind_result($NumRecs, $this->LastDate, $this->Requester);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
		if ($NumRecs == 0) {
			return true;
		}
		else {
			return false;
		}
	}

	private function checkFirstTimeUse() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT count(*), `ServerDongle` FROM `SDServer` WHERE `Status` = 1 AND `ServerString` = ?");
		$query->bind_param("s", $this->ServerString);
		$query->bind_result($NumRecs, $ServerName);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
		if ($NumRecs == 0) {
			$this->ServerDongle = $ServerName;
			return true;
		}
		else {
			$this->StringCount  = $NumRecs;
			return false;
		}
	}

	private function getDetails() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT `ServerDongle`, `CoyId`, `CoyBranch` FROM `SDServer` WHERE `Status` = 1 AND `ServerString` = ? AND `ModeId` = ?");
		$query->bind_param("si", $this->ServerString, $this->UnlockModeId);
		$query->bind_result($this->ServerDongle, $this->CompanyId, $this->Branch);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
	}

	private function getApp() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT `App_Desc`, `App_Code` FROM `Application` WHERE `App_Lic_Code` = ?");
		$query->bind_param("s", $this->AppCode);
		$query->bind_result($this->AppDesc, $this->AppAbbreviation);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
	}

	private function checkStatus() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT `Status`, `ServerDongle` FROM `ServerDongle` WHERE `ServerString` = ?");
		$query->bind_param("s", $this->ServerString);
		$query->bind_result($this->Status, $this->ServerDongle);
		$query->execute();
		$query->fetch();
		$query->close();
		$con->close();
	}

	private function getUnlockModeList() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT `id`, `Code`, `Desc` FROM `SDMode`");
		$query->bind_result($id, $Code, $Desc);
		$query->execute();
		while ($query->fetch()) {
			$this->UnlockModeList[$Code] = array ('Id' => $id, 'Desc' => $Desc);
		}
		$query->close();
		$con->close();
	}

	private function getCoyList() {
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("SELECT `Coy_Id`, AES_DECRYPT(`Coy_Code`, concat('iiAdamDbnChigimoo1!', `Coy_Id`)), 
									AES_DECRYPT(`Coy_Desc`, concat('iiAdamDbnChigimoo1!', `Coy_Id`))  
									FROM `Company` 
									WHERE `IsActive` = 1;");
		$query->bind_result($Id, $Code, $Desc);
		$query->execute();
		while ($query->fetch()) {
			$this->CoyList[$Id] = array ('$Code' => $Code, 'Desc' => $Desc);
		}
		$query->close();
		$con->close();
	}

	private function saveSerial() {
		$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
		include 'conn.var.php';
		$con   = new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		$query = $con->prepare("INSERT `SDSerial` (`Serial`, `Status`, `CreateUser`, `CreateDate`) VALUES (?, 1, ?, ?)");
		$query->bind_param("sis", $this->Serial, $_SESSION['User_Id'], $Today);
		if($query->execute()) {$Ret = true;}
		else {$Ret = false;}
		$query->close();
		$con->close();
		return $Ret;
	}
}

?>
