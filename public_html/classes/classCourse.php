<?php
//Google API list calendar events...
//
//curl \
//  'https://www.googleapis.com/calendar/v3/calendars/inspired.interfaces%40gmail.com/events?maxResults=100&orderBy=startTime&singleEvents=true&timeMax=2018-09-30T23%3A59%3A59%2B02%3A00&timeMin=2018-09-01T00%3A00%3A00%2B02%3A00' \
//  --header 'Authorization: Bearer [YOUR_BEARER_TOKEN]' \
//  --header 'Accept: application/json' \
//  --compressed

/**
 * Description of classAdmin
 *
 * @author rory
 */
class classCourse extends mysqli {

	protected $UserId;
	protected $UserData;
	protected $CourseList;
	protected $SelectedCourse;
	protected $AppList;
	protected $TrainerList;
	protected $VenueList;
	protected $StatusList = array("Inactive", "Active", "Closed", "Complete");
	protected $VenueListDetail;

	public function __construct($UserId) {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->UserId = $UserId;
		$this->listCourses();
		$this->getLists();
	}
	
	private function getLists() {
		// ================ App ================
		$sqlA       = "SELECT `App_Id`,     `App_Code`, `App_Desc` FROM `App` ORDER BY `App_View`";
		$result     = $this->query($sqlA);
		while ($row = mysqli_fetch_array($result)) {
			$this->AppList[] = $row;
		}
		mysqli_free_result($result);
		// ================ Trainer ================
		$sqlT       = "SELECT `Trainer_Id`, concat(`Trainer_SName`, ', ', `Trainer_FName`) FROM `Trainer`";
		$result = $this->query($sqlT);
		while ($row = mysqli_fetch_array($result)) {
			$this->TrainerList[] = $row;
		}
		mysqli_free_result($result);
		// ================ Venue ================
		$sqlV       = "SELECT `Venue_Id`, `Venue_Code`, `Venue_Desc` FROM `Venue` ORDER BY `Venue_Display`";
		$resultV    = $this->query($sqlV);
		$this->VenueList = mysqli_fetch_all($resultV);
		// ================ Venue Detail ================
		$sqlVList   = "SELECT `Venue_Id`, `Venue_Code`, `Venue_Name`, `Venue_Desc`, `Venue_City`, `Venue_Province`, `Venue_Short` FROM `Venue` ORDER BY `Venue_Display`";
		$resultList = $this->query($sqlVList);
		while ($row = mysqli_fetch_assoc($resultList)) {
			$this->VenueListDetail[] = $row;
		}
		mysqli_free_result($resultV);
		mysqli_free_result($resultList);
	}
	
	private function listCourses() {
		$sql  = "SELECT C.Course_Id, C.Course_Number 'Number', C.Course_Start_Date 'Start', C.Course_End_Date 'End', A.App_Desc 'App', T.Trainer_Code Trainer, V.Venue_Desc 'Venue', C.Course_Status 'Status' "
			. "FROM Course C "
			. "JOIN App A "
			. "ON (A.App_Id = C.Course_App_Id) "
			. "JOIN Trainer T "
			. "ON (T.Trainer_Id = C.Course_Trainer_Id) "
			. "JOIN Venue V "
			. "ON (V.Venue_Id = C.Course_Venue_Id)  "
			. "ORDER BY C.Course_Start_Date DESC";
//		echo $sql;
		$result = $this->query($sql);
		while ($row = mysqli_fetch_assoc($result)) {
			$this->CourseList[$row['Course_Id']] = $row;
		}
		mysqli_free_result($result);
	}
	
	protected function getCourseDetail($CourseId) {
		$sql  = "SELECT C.Course_Id, C.Course_Number, C.Course_Start_Date, C.Course_End_Date, "
				. "A.`App_Id`, T.`Trainer_Id`, V.`Venue_Id`, C.Course_Status FROM Course C "
				. "JOIN App A ON (A.App_Id = C.Course_App_Id) "
				. "JOIN Trainer T ON (T.Trainer_Id = C.Course_Trainer_Id) "
				. "JOIN Venue V ON (V.Venue_Id = C.Course_Venue_Id) "
				. "WHERE C.Course_Id = $CourseId "
				. "ORDER BY C.Course_Start_Date DESC";
		$result = $this->query($sql);
		$row = mysqli_fetch_assoc($result);
		$this->SelectedCourse = $row;
		mysqli_free_result($result);
		return $this->SelectedCourse;
	}


	private function getUserInfo($UserId) {
		$sql = "SELECT `Per_Id`, `Per_Desc`, `Per_Name`, `Per_Level` 
				FROM `Permissions` 
				WHERE `Per_Id` = $UserId";
		$result = $this->query($sql);
		$this->UserData = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
	}

	protected function getNextCourseNumber($startDate) {
		$year = substr($startDate, 2, 2);
		$sql = "SELECT `Course_Number` FROM `Course` WHERE LEFT(`Course_Number`, 2) = '$year' ORDER BY `Course_Number` DESC LIMIT 1";
		$result = $this->query($sql);
		$number = mysqli_fetch_array($result);
		$LastNumber = $number[0];
		if (substr($LastNumber, 0, 2) !== $year) {
			$NextCourseNumber = $year . '01';
		}
		else {
			$NextCourseNumber = $LastNumber + 1;
		}
		return $NextCourseNumber;
	}
	
	protected function doUpdateCourse($courseId, $courseData, $job) {
		$ret = FALSE;
//		Array ( 
//		[0] => stdClass Object ( [id] => Course_Id [value] => New ) 
//		[1] => stdClass Object ( [id] => Course_Number [value] => 1806 ) 
//		[2] => stdClass Object ( [id] => Course_Start_Date [value] => 2018-11-13 ) 
//		[3] => stdClass Object ( [id] => Course_End_Date [value] => 2018-11-15 ) 
//		[4] => stdClass Object ( [id] => App_Id [value] => 20 ) 
//		[5] => stdClass Object ( [id] => Trainer_Id [value] => 0 ) 
//		[6] => stdClass Object ( [id] => Venue_Id [value] => 97 ) 
//		[7] => stdClass Object ( [id] => Course_Status [value] => 1 ) )
		if ($courseId === "New") {
//			echo "New<br>";
			$sql = "INSERT `Course` "
				. "(`Course_Venue_Id`, `Course_App_Id`, `Course_Trainer_Id`, `Course_Number`, `Course_Start_Date`, `Course_End_Date`, `Course_Status`, `Course_Create_Date`, `Course_Create_User`, `Course_Mod_Date`, `Course_Mod_User`)"
				. "VALUES('"
				. $courseData[6]->value . "', '" //venue
				. $courseData[4]->value . "', '" //app
				. $courseData[5]->value . "', '" //trainer
//				. $courseData[1]->value . "', '" // number
				. $this->getNextCourseNumber($courseData[2]->value) . "', '" // number
				. $courseData[2]->value . "', '" //start
				. $courseData[3]->value . "', '" //end
				. $courseData[7]->value . "', '" //status
				. TODAY . "', '" // Create_Date
				. $this->UserId . "', '" // Create_User
				. TODAY . "', '" // Mod_Date
				. $this->UserId . "')"; // Mod User  
		}
		elseif ($job === "Delete") {
			// TODO =====> Check for existing students first!
			if (!hasLearners($courseId)) {
//			$sql = "DELETE FROM `Course` WHERE `Course_Id` = $courseId";
			echo "<br>Can Delete<br>";
			}
		}
		else {
			$sql = "UPDATE `Course`"; 
			$sql .= "SET `Course_Venue_Id` = '" . $courseData[6]->value . "', "
				. "`Course_App_Id` = '"     . $courseData[4]->value . "', " // app
				. "`Course_Trainer_Id` = '" . $courseData[5]->value . "', " // trainer
				. "`Course_Number` = '"     . $courseData[1]->value . "', " // number
				. "`Course_Start_Date` = '" . $courseData[2]->value . "', " // start
				. "`Course_End_Date` = '"   . $courseData[3]->value . "', " // end
				. "`Course_Status` = '"     . $courseData[7]->value . "', " // status
				. "`Course_Mod_Date` = '"   . TODAY . "', "				    // Mod_Date
				. "`Course_Mod_User` = " . $this->UserId				    // Mod_User
				. " WHERE `Course_Id` = $courseId";
		}
		if ($this->query($sql)) {
			$ret = TRUE;
		}
		return $ret; // ret = true or false on query success
	}
	
	protected function findClash($ClashType, $ClashId, $ClashStart, $ClashEnd) {
		// @findClash returns 0 or 1 to ajax checkClashes() to indicate trainer double booking
		$ClashField = 'Course_' . $ClashType;
		$sql = "SELECT count(*) as 'CourseCount'
				FROM `Course` c
				WHERE c.`Course_Start_Date` >= '$ClashStart'
				AND   c.`Course_Start_Date` <= '$ClashEnd'
				AND   c.`Course_End_Date` >= '$ClashStart'
				AND   c.`Course_End_Date` <= '$ClashEnd'
				AND   c.`Course_Status` = 1 
				AND	  c.`$ClashField` = $ClashId
				ORDER BY c.`Course_Start_Date`";
//		echo $sql;
		$result = $this->query($sql);
		$row = mysqli_fetch_assoc($result);
		mysqli_free_result($result);
		echo $row['CourseCount']; // does clash exist? 0 = false, 1 = true
	}
	
	protected function hasLearners($courseId) {
		$sql = "SELECT count(*) as 'Students' 
				FROM `Course_Attendee` 
				WHERE `Course_Id` = $courseId";
		$result = $this->query($sql);
		$row = mysqli_fetch_array($result);
		if ($row['Students'] === 0) {
			return FALSE;
		}
		else {
			return $row['Students'];
		}
		mysqli_free_result($result);
	}
	
}

?>
