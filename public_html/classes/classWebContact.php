<?php
/**
 * Description of classWebContact
 * 
 * Web contact form only
 * Generates list and marks as responded
 *
 * @author rory
 */
class classWebContact extends mysqli {

	private $webContacts = array();
	private $key_str = "ThisIsMyEncryptionKey!";
	
	public function __construct() {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->fetchWebContacts();
	}
	
	protected function getWebContacts() {
		return $this->webContacts;
	}
	
	protected function doWebContactUpdate($rowId, $checked) {
		$ret = FALSE;
		if ($checked) {
			$sql = "UPDATE `webContact` "
					. "SET `responseStatus` = 1 "
					. "WHERE `id` = $rowId";
		}
		if ($result = $this->query($sql)) {
			$ret = TRUE;
		}
		return $ret;
	}
	
	private function fetchWebContacts() {
		$sql = "SELECT `id`, "
				. "AES_DECRYPT(`type`, '" . $this->key_str . "') as 'type', "
				. "AES_DECRYPT(`name`, '" . $this->key_str . "') as 'name', "
				. "AES_DECRYPT(`email`, '" . $this->key_str . "') as 'email', "
				. "AES_DECRYPT(`telephone`, '" . $this->key_str . "') as 'telephone', "
				. "AES_DECRYPT(`mobile`, '" . $this->key_str . "') as 'mobile', "
				. "AES_DECRYPT(`company`, '" . $this->key_str . "') as 'company', "
				. "AES_DECRYPT(`city`, '" . $this->key_str . "') as 'city', "
				. "AES_DECRYPT(`ReticMaster`, '" . $this->key_str . "') as 'ReticMaster', "
				. "AES_DECRYPT(`PowaMaster`, '" . $this->key_str . "') as 'PowaMaster', "
				. "AES_DECRYPT(`PowerOffice`, '" . $this->key_str . "') as 'PowerOffice', "
				. "AES_DECRYPT(`number`, '" . $this->key_str . "') as 'number', "
				. "AES_DECRYPT(`message`, '" . $this->key_str . "') as 'message', "
				. "`responseStatus`, `createDate`, `responseDate`, `completeDate`"
				. " FROM `webContact` WHERE `responseStatus` = 0 ORDER BY `CreateDate` DESC";
		$result = $this->query($sql);
		while ($data = mysqli_fetch_assoc($result)) {
			$this->webContacts[$data['id']] = $data;
		}
	}
}

?>
