<?php

/**
 * Description of classAdmin
 *
 * @author rory
 */
class classIssue extends mysqli {

	private	  $UserId;
	protected $appList;
	protected $statusList;
	protected $reportList;
	protected $progressList;
	protected $selectedReportProgress;

	public function __construct($UserId) {
		$this->UserId = $UserId;
		$this->connect(DB_HOST, DB_USER, DB_PASS, 'c350324_Status');
		$this->getMasters();
	}
	
	private function getMasters() {
		$sqlA       = "SELECT `appId`,    `appCode`,    `appDesc` FROM `app`";
		$sqlS       = "SELECT `statusId`, `statusCode`, `statusDesc` FROM `status`";
		$result     = $this->query($sqlA);
		while ($row = mysqli_fetch_array($result)) {
			$this->appList[] = $row;
		}
		mysqli_free_result($result);
		$result = $this->query($sqlS);
		while ($row = mysqli_fetch_array($result)) {
			$this->statusList[] = $row;
		}
		mysqli_free_result($result);
	}
	
}

?>
