<?php


trait traitCourse {
	
	protected function fetchStudentCourseList($StudentId) { // Courses attended and enrolled in
		$sql = "SELECT c.`Course_Id`, c.`Course_Number`, a.`App_Desc`, c.`Course_Start_Date`, v.`Venue_Short`, t.`Trainer_Code`
				FROM `Course_Attendee` ca
				JOIN `Course` c ON (ca.`Course_Id` = c.`Course_Id`)
				JOIN `App` a ON (c.`Course_App_Id` = a.`App_Id`)
				JOIN `Venue` v ON (c.`Course_Venue_Id` = v.`Venue_Id`)
				JOIN `Trainer` t ON (c.`Course_Trainer_Id` = t.`Trainer_Id`)
				WHERE ca.`User_Id` = $StudentId
				ORDER BY c.`Course_Start_Date` DESC";
//		echo "<br>$sql<br>";
		return $this->fetchList($sql);
	}
}

?>
