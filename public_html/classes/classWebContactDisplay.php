<?php
	include CLASSES . 'classWebContact.php';
/**
 * Description of classWebContactDisplay
 * 
 * Web Form Contacts only
 *
 * @author rory
 */
class classWebContactDisplay extends classWebContact {

	private $webContacts;
//	Array ( [0] => id [1] => type [2] => name [3] => email [4] => telephone [5] => mobile [6] => company [7] => city [8] => ReticMaster [9] => PowaMaster [10] => PowerOffice [11] => number [12] => message [13] => responseStatus [14] => createDate [15] => responseDate [16] => completeDate ) 
	
	public function __construct() {
		parent::__construct();
	}
	
	public function webContacts() {
		$this->webContacts = $this->getWebContacts();
		$check = "<i class='fa fa-check'></i>";
		$tableTop  = "<table class='table table-hover table-border webContacts'>";
		$tableTop .= "<thead><tr>";
		$tableTop .= "<th>Name</th>";
		$tableTop .= "<th>Company</th>";
		$tableTop .= "<th>City</th>";
		$tableTop .= "<th>Type</th>";
		$tableTop .= "<th class='text-center'>Attendees</th>";
		$tableTop .= "<th>RM</th>";
		$tableTop .= "<th>PM</th>";
		$tableTop .= "<th>PO</th>";
		$tableTop .= "<th>Date</th>";
//		$tableTop .= "<th>Status</th>";
		$tableTop .= "<th class='text-center'>Responded</th>";
		$tableTop .= "</tr></thead>";
		$tableBody = '';
		$tableFoot = "</table>";
		$tableMessage = "<table class='table table-hover table-border webContacts'>"
				. "<tr title='This is how your message will show.'>"
				. "<td class='message' colspan='10'>Yellow Highlight indictes message => hover to reveal.</td></tr></table>";

		foreach ($this->webContacts as $Key => $data) {
			$responseStatus = $data['responseStatus'];
			$tableBody .= "<tr id='$Key'";
			if (isset($data['message'])) {
				$tableBody .= " class='message' title='" . $data['message'] . "'";
			}
			$tableBody .= ">";
			$tableBody .= "<td>" . $data['name'] . "</td>";
			$tableBody .= "<td>" . $data['company'] . "</td>";
			$tableBody .= "<td>" . $data['city'] . "</td>";
			$tableBody .= "<td>" . $data['type'] . "</td>";
			$tableBody .= "<td class='text-center'>" . $data['number'] . "</td>";
			$tableBody .= "<td>";
				if (isset($data['ReticMaster'])) {$tableBody .= $check;}
			$tableBody .= "</td>";
			$tableBody .= "<td>";
				if (isset($data['PowaMaster'])) {$tableBody .= $check;}
			$tableBody .= "</td>";
			$tableBody .= "<td>";
				if (isset($data['PowerOffice'])) {$tableBody .= $check;}
			$tableBody .= "</td>";
			$tableBody .= "<td>" . $data['createDate'] . "</td>";
//			$tableBody .= "<td>" . $data['responseStatus'] . "</td>";

			$tableBody .= "<td class='text-center'>";
			$tableBody .= "<input type='checkbox' id='chbResp$Key' ";
			if ($data['responseStatus'] == 1) {echo " checked ";}
			$tableBody .= "></td>";
			$tableBody .= "</tr>";
			
		}
		echo $tableMessage . $tableTop . $tableBody . $tableFoot;
	}
	
	public function webContactUpdate($rowId, $checked) {
		if (is_numeric($rowId) && $checked) {
			if ($this->doWebContactUpdate($rowId, $checked)) {
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
	}

}

?>
