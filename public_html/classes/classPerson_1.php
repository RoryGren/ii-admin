<?php
include_once CLASSES . 'traitCRUD.php';
/**
 * Description of classPerson
 *
 * Attributes:
 * FName, SName, IDNumber
 * 
 * @author rory
 */
class classPerson extends mysqli {
	use traitCRUD;

	protected $userId;
	protected $userList;
	protected $userIdList;
	protected $CoyList;
	protected $BranchList;
	protected $AppList;
	private $selectedPerson;
	
	public function __construct($UserId) {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->userId = $UserId;
		$this->fetchUserIdList();
		$this->getAttribLists();
	}
	
	protected function userList() {
		self::fetchUserList();
		return $this->userList;
	}

	protected function checkUserId($IDNo) { // returns new or rowId for existing user 
		if (array_key_exists($IDNo, $this->userIdList)) {
			$RetVal = $this->userIdList[$IDNo];
			return $RetVal;
		}
		else {
			return 'new';
		}
	}
	
	private function fetchUserIdList() {
		$sql = "SELECT 
				`User_Id_Key`, 
				AES_DECRYPT(`User_Id_Number`, concat('" . DB_KEY . "', `User_Id_Key`)) 'User_Id_Number' 
				FROM `User`";
		$result = $this->query($sql);
		while ($row = mysqli_fetch_array($result)) {
			$this->userIdList[$row[1]] = $row[0];
		}
		mysqli_free_result($result);
	}
	
	protected function returnUserList() {
		$this->fetchUserList();
		return $this->userList;
	}
	
	protected function getAttribLists() {
		// ================ Coy ================
		$this->CoyList     = $this->fetchList("SELECT `Coy_Id`, `Coy_Code`, `Coy_Desc` FROM `Company` ORDER BY `Coy_Desc`");
		// ================ Branch ================
		$this->BranchList  = $this->fetchList("SELECT b.`Branch_Id`, b.`Branch_Code`, b.`Branch_Desc` FROM `Branch` b ORDER BY b.`Branch_Desc`");
		// ================ App ================
		$this->AppList = $this->fetchList("SELECT `App_Id`, `App_Code`, `App_Desc` FROM `App` ORDER BY `App_View`");
		// ================ Course ================
		$this->CourseList = $this->fetchList("SELECT C.Course_Id, concat(A.`App_Desc`, ' - ', C.`Course_Start_Date`, ' - ',  V.`Venue_Desc`) AS 'Course', C.`Course_App_Id` "
			. "FROM Course C "
			. "JOIN `App` A ON (C.Course_App_Id = A.App_Id)  "
			. "JOIN Venue V "
			. "ON (V.Venue_Id = C.Course_Venue_Id)  "
			. "ORDER BY C.Course_Start_Date DESC");
	}
	

	private function fetchUserList() {
		$sql = "SELECT "
				. "u.`User_Id_Key`"
				. ", AES_DECRYPT(u.`User_Last`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Last'"
				. ", AES_DECRYPT(u.`User_First`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_First'"
				. ", AES_DECRYPT(u.`User_Nick`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Nick'"
				. ", AES_DECRYPT(u.`User_Id_Number`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Id_Number'"
				. ", AES_DECRYPT(u.`User_Co_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Co_Id'"
				. ", AES_DECRYPT(u.`User_Branch_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Branch_Id'"
				. ", u.`Att_Id`"
				. ", AES_DECRYPT(u.`User_Email`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Email'"
				. ", AES_DECRYPT(u.`User_Cel`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Cel'"
				. ", AES_DECRYPT(u.`User_Tel`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Tel'
					, c.`Coy_Desc`
					, b.`Branch_Desc`
					, u.`isActive`
				FROM `User` u
				LEFT JOIN `Company` c ON (AES_DECRYPT(u.`User_Co_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) = c.`Coy_Id`)
				LEFT JOIN `Branch` b ON (AES_DECRYPT(u.`User_Branch_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) = b.`Branch_Id`)
				ORDER BY `User_Last`, `User_First`";
		$this->userList = $this->fetchList($sql);
//		echo "<br>$sql<br>";
	}

}

?>
