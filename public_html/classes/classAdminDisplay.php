<?php
include dirname(__FILE__) . '/classAdmin.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of classAdminDisplay
 *
 * @author rory
 */
class classAdminDisplay extends classAdmin {
	
	public function __construct($User) {
		parent::__construct($User);
//		self::function();
	}
	
	public function userList() {
		$UserList = $this->returnUserList();
		$TableHeader = "<table id='tableList' class='table table-hover table-striped table-bordered table-responsive table-list text-small'>"
			. "<thead><tr>";
		$TableHeader .= "<th>Login Name</th><th>Name</th><th>Code</th>";
//		$TableHeader .= "<td>&nbsp;</th>";
		$TableHeader .= "</tr></thead>";
		echo $TableHeader;
		$TableBody = "<tbody>";
		foreach ($UserList as $key => $row) {
//			[Per_Id] => 6 [Per_Desc] => Brad [Per_Name] => Brad [Per_Short] => BL [Per_Level] => 0
			$rowClass = '';
			if ($row['Per_Level'] == 0) {$rowClass = " class='inactive'";}
			$TableBody .= "<tr $rowClass id='" . $row['Per_Id'] . "'>";
			$TableBody .= "<td>" . $row['Per_Desc'] . "</td>";
			$TableBody .= "<td>" . $row['Per_Name'] . "</td>";
			$TableBody .= "<td>" . $row['Per_Short'] . "</td>";
//			$TableBody .= "<td>";
//			$TableBody .= "<button class='btn btn-small btn-primary pwdReset'>Reset Password</button>&nbsp;";
//			$TableBody .= "<button class='btn btn-small btn-danger pwdRevoke'>Remove Access</button>";
//			$TableBody .= "</td>";
			
			$TableBody .= "</tr>";
			
//			print_r($row);
//			echo "<br><br>";
		}
		echo $TableBody . "</tbody></table><button class='btn btn-primary btnNew' id='btnNew'>New</button>";
	}

	public function userDetails($rowId) {
		$UserList = $this->returnUserList();
		return $UserList[$rowId];
	}
	
	public function updateUserData($rowId, $info) {
		$this->updateUser($rowId, $info);
	}
	
}

?>
