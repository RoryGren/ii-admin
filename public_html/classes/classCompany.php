<?php
include dirname(__FILE__) . '/traitCRUD.php';
include dirname(__FILE__) . '/traitTable.php';
/**
 * Description of classCompany
 *
 * @author rory
 */
class classCompany extends mysqli {
	use traitCRUD;
//	use traitTable;
	
	private	  $UserId;
	protected $CompanyList;
	protected $SelectedCompanyId;
	protected $Branch;
	protected $PrettyHeaders;


	public function __construct($UserId) {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->UserId = $UserId;
		$this->fetchCompanyList();
	}

	private function getList($sql) {
		$result     = $this->query($sql);
		while ($row = mysqli_fetch_assoc($result)) {
			$List[$row['Id']] = $row;
		}
		mysqli_free_result($result);
		return $List;
	}
	
	private function fetchCompanyList() {
		$sql = "SELECT "
					. "c.`Coy_Id`"
					. ", AES_DECRYPT(c.`Coy_Code`, concat('" . DB_KEY . "', c.`Coy_Id`)) AS 'Code'"
					. ", AES_DECRYPT(c.`Coy_Desc`, concat('" . DB_KEY . "', c.`Coy_Id`)) AS 'Company'" 
					. ", c.`IsActive` "

					. ", b.`Branch_Id`"
					. ", AES_DECRYPT(b.`Branch_Code`, concat('" . DB_KEY . "', b.`Branch_Id`)) 'Branch_Code'"
					. ", AES_DECRYPT(b.`Branch_Desc`, concat('" . DB_KEY . "', b.`Branch_Id`)) 'Branch'"
					. ", b.`IsActive` "

					. "FROM `Company` c "
					. "JOIN `Branch` b ON (AES_DECRYPT(b.`Coy_Id`, concat('" . DB_KEY . "', b.`Branch_Id`)) = c.`Coy_Id`) "
					. "WHERE c.`Coy_Id` > 1 "
					. "ORDER BY Company, Branch;";
//		echo "<br>$sql<br>";
		$List = [];
		$result = $this->query($sql);
//		echo "<br>";
		while ($row = mysqli_fetch_assoc($result)) {
//			$List[$row['Coy_Id']] = $row;
			$List[$row['Branch_Id']] = $row;
//			print_r($List);
//			echo "<br>";
		}
		mysqli_free_result($result);

//		$this->CompanyList = $this->fetchListAssoc($sql);
		$this->CompanyList = $List;
		
	}
	
	protected function fetchBranch($branchId) {
		$sql = "SELECT 
			AES_DECRYPT(`Branch_Code`, concat('" . DB_KEY . "', `Branch_Id`)) 'Branch_Code',  
			AES_DECRYPT(`Branch_Desc`, concat('" . DB_KEY . "', `Branch_Id`)) 'Branch_Desc', 
			`IsActive` 
			FROM `Branch`
			WHERE `Branch_Id` = $branchId";
		$result = $this->query($sql);
		$this->Branch = mysqli_fetch_assoc($result);
		return $this->Branch;
	}
	
	protected function fetchBranchDetail($branchId) {
		if (isset($branchId)) {
			$sql = "SELECT 
				v.`AttribValId`, 
				a.`AttribId`, 
				v.`ParentId`, 
				AES_DECRYPT(v.`AttribVal`, concat('" . DB_KEY . "', v.`ParentId`)) 'AttribVal', 
				a.`AttribDesc`, 
				a.`AttribDisplayOrder`, 
				a.`isMandatory`, 
				a.`AttribStatus`
				FROM `Attrib` a
				LEFT OUTER JOIN `AttribVal` v ON (a.`AttribId` = v.`AttribId` AND v.`ParentId` = $branchId)
				WHERE a.`AttribCat` = 1
				ORDER BY a.`AttribDisplayOrder`";
		}
		else {$sql = "SELECT 'new'as `AttribValId`, a.`AttribDesc`, a.`AttribDisplayOrder`, a.`isMandatory`, a.`AttribStatus` FROM `Attrib` a WHERE a.`AttribCat` = 1 ORDER BY a.`AttribDisplayOrder`";}
//		echo "<br>$sql<br>";
		$query = $this->query($sql);
		while ($row = mysqli_fetch_array($query)) {
			$branchDetail[] = $row;
		}
		mysqli_free_result($query);
		return $branchDetail;
	}
	
}

?>
