<?php
include_once 'traitCRUD.php';
//include_once 'classCourse.php';
include_once 'classPerson.php';
/**
 * Description of classStudent
 *
 * @author rory
 */
class classStudent extends classPerson {
	
	use traitCRUD;
	
	private $StudentList; // array of student info
	protected $UserIdList;  // array of User ID Numbers
//	protected $CoyList;
//	protected $BranchList;
//	protected $AppList;
	protected $CourseList;
	private	  $courseAttendeeList;

	public function __construct($UserId) {
		parent::__construct($UserId);
//		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->fetchStudentList();
//		$this->getAttribLists();
	}
	
	protected function returnStudentList() {
		$this->fetchStudentList();
		return $this->StudentList;
	}
	
	private function fetchStudentList() {
		// TODO =====> Deprecate Attendee table - Att_Cert is course specific.
		$sql = "SELECT "
				. "u.`User_Id_Key`"
				. ", AES_DECRYPT(u.`User_Last`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Last'"
				. ", AES_DECRYPT(u.`User_First`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_First'"
				. ", AES_DECRYPT(u.`User_Nick`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Nick'"
				. ", AES_DECRYPT(u.`User_Id_Number`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Id_Number'"
				. ", AES_DECRYPT(u.`User_Co_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Co_Id'"
				. ", AES_DECRYPT(u.`User_Branch_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Branch_Id'"
				. ", u.`Att_Id`"
				. ", AES_DECRYPT(u.`User_Email`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Email'"
				. ", AES_DECRYPT(u.`User_Cel`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Cel'"
				. ", AES_DECRYPT(u.`User_Tel`, concat('" . DB_KEY . "', u.`User_Id_Key`)) 'User_Tel'
					, AES_DECRYPT(c.`Coy_Desc`, concat('" . DB_KEY . "', c.`Coy_Id`)) AS 'Coy_Desc'
					, b.`Branch_Desc`
					, AES_DECRYPT(a.`Att_Cert`, concat('iiAdamDbnChigimoo1!', u.`User_Id_Key`)) 'Att_Cert' 
					, u.`isActive`
				FROM `User` u
				LEFT JOIN `Company` c ON (AES_DECRYPT(u.`User_Co_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) = c.`Coy_Id`)
				LEFT JOIN `Branch` b ON (AES_DECRYPT(u.`User_Branch_Id`, concat('" . DB_KEY . "', u.`User_Id_Key`)) = b.`Branch_Id`)
				LEFT JOIN `Course_Attendee` a ON (u.`Att_Id` = a.`Attendee_Id`)
				ORDER BY `User_Last`, `User_First`";
		$this->StudentList = $this->fetchList($sql);
//		echo "<br>$sql<br>";
	}
	
	protected function checkId($IdNo) {
		// Unused??
		echo "<h1>classStudent::checkId($IdNo)</h1><br>";
//		$sql = "SELECT u.`User_Id_Key` 
//				FROM `User` u 
//				WHERE AES_DECRYPT(u.`User_Id_Number`, concat('iiAdamDbnChigimoo1!', `User_Id_Key`)) = '$IdNo'";
//		$result = $this->fetchList($sql);
//		print_r(array_keys($result));
		echo $this->checkUserId($IdNo);

//		if (count($result) > 0) {
//			return array_keys($result);
//		}
//		else {
//			return FALSE;
//		}
	}
	
	protected function checkFirstLast($FName, $SName) { // UNUSED...
		echo "classStudent::checkFirstLast()<br>";
		
	}
	
	protected function updateStudentRecord($recordId, $info, $UserId, $job, $table) {

// =============================================================================
// ================= Debugging =================================================
// =============================================================================
//		echo "<br>updateStudentRecord()<br>";
//		=====> Indexes change depending on which form data is retrieved from ===
//		=====> convert JSON array of objects into array of field => value ======
		foreach ($info as $key => $data) {
			$formData[$data->id] = $data->value;
		}
//	Students: [Student_Id]								    [User_Id_Number] [User_Last] [User_First] [User_Nick] [User_Co_Id] [Att_Cert] [User_Email] [User_Tel] [User_Cel] [senderParent] ) 
//	Courses:  [Student_Id] [Selected_Course_Id] [Course_Id] [User_Id_Number] [User_Last] [User_First] [User_Nick] [User_Co_Id] [Att_Cert] [User_Email] [User_Tel] [User_Cel] [senderParent] )
//		echo "<br>formData array:<br>";
//		print_r($formData);
//		echo "<br>";
		
		
//		foreach ($formData as $key => $value) {
//			echo "$key => $value<br>";
//		}
		
// =============================================================================
// =============================================================================
// =============================================================================
		
		// ===== Initialise =====
		$fields = '';

		if ($recordId === 'new') {
			
		// ===== Create new User Record and get rowId =====
			
			$sqlNew = "INSERT INTO `User` (`User_Id_Number`, `isActive`, `CreateUser`, `CreateDate`, `ModUser`, `ModDate`) "
							. "values('" . $formData['User_Id_Number'] . "', 1, $UserId, '" . TODAY . "', $UserId, '" . TODAY . "')";
			$recordId = $this->insertRecord($sqlNew);
		}
		
		// ===== Use rowId to update User Record with new or changed data =====

		foreach($formData as $field => $value) {
			if ($field !== 'Student_Id' && $field !== 'Course_Id' && $field !== 'Selected_Course_Id' && $field !== 'Att_Cert' && $field !== 'senderParent') {
				$fields  .= "`" . $field . "` = AES_ENCRYPT('" . $value . "', concat('" . DB_KEY . "', '$recordId')), ";
			}
		}
		$fields  .= "`ModUser` = '$UserId', `ModDate` = '" . TODAY . "'";
		$sqlUpdate = "UPDATE `User` SET $fields WHERE `User_Id_Key` = '$recordId'";

//		echo "<br>UpdateSQL:<br>$sqlUpdate<br>";
		
		$this->updateRecord($sqlUpdate);
		
		// ========== Check if User has Attendee record ========================
		
		$attendeeId = $this->attendeeExists($recordId);

		// ===== If User not in Attendee && senderParent is Course, ============
		// ===== add record to Attendee ===================
		// ===== add attendeeId to User record  ===================
		if ( $formData['senderParent'] === 'Course') {
			if (is_null($attendeeId)) { 
		// TODO =====> Deprecate Attendee table..... Att_Cert is course specific
				$sqlAttendeeNew = "INSERT INTO `Attendee` (`Is_Active`, `CreateUser`, `CreateDate`, `ModUser`, `ModDate`) "
						. "VALUES(1, " . $this->userId . ", '" . TODAY . "', " . $this->userId . ", '" . TODAY . "')";
				$attendeeId = $this->insertRecord($sqlAttendeeNew); // insert to get new rowId

				$sqlUserUpdate = "UPDATE `User` SET `Att_Id` = '$attendeeId' WHERE `User_Id_Key` = '$recordId'";
				$this->updateRecord($sqlUserUpdate);
			}
		}

		// ========== Update Att_Cert in Attendee ==============================
		// TODO =====> Deprecate Attendee table.....

		$sqlAttendeeUpdate = "UPDATE `Attendee` "
				. "SET `Att_Cert` = AES_ENCRYPT('" . $formData['Att_Cert'] . "', concat('" . DB_KEY . "', '$attendeeId')), "
				. "`ModUser` =" . $this->userId . ", `ModDate` = '" . TODAY . "' "
				. " WHERE `Attendee_Id` = $attendeeId";
		$this->updateRecord($sqlAttendeeUpdate);

		// ===== Insert Course_Attendee record if not already there ============
		// ===== Only if "Add To Course" has been selected from Course List ====
		// ===== I.e senderParent === 'Course' =================================

		if ($this->userNotInCourse($recordId, $formData['Selected_Course_Id']) && $formData['senderParent'] === 'Course') { 
			$sqlCourseAttendee = "INSERT INTO `Course_Attendee` (`Course_Id`, `Attendee_Id`, `User_Id`, `Coy_Id`, `Att_Cert`) "
					. "VALUES("
					. $formData['Selected_Course_Id'] . ", "
					. "$attendeeId, "
					. "$recordId, " 
					. $formData['User_Co_Id'] . ", " 
					. "AES_ENCRYPT('" . $formData['Att_Cert'] . "', concat('" . DB_KEY . "', '$recordId'))" 
					. ")";
			$this->updateRecord($sqlCourseAttendee);
		}
	}
	
	private function attendeeExists($recordId) {
		// =====> Use rowId to find Attendee_Id or create new Attendee Record =====
		// =====> Returns Attendee_Id or null =====
		$sql    = "SELECT `Att_Id` FROM `User` WHERE `User_Id_Key` = $recordId";
		$result = $this->query($sql);
		$record = mysqli_fetch_array($result);
		$attId  = $record[0];
		return $attId;
	}
	
	protected function userNotInCourse($userRowId, $courseRowId) {
//		echo "<br>userNotInCourse($userRowId, $courseRowId)<br>";
		$sqlInCourseCheck = "SELECT count(*) AS 'Recs' FROM `Course_Attendee` WHERE `Course_Id` = '$courseRowId' AND `User_Id` = '$userRowId'";
//		echo "<br>$sqlInCourseCheck<br>";
		$result = $this->query($sqlInCourseCheck);
		$record = mysqli_fetch_array($result);
//		echo "<br>record: userNotInCourse :: ";
//		var_dump($record);
//		echo "<br>";
		if ($record['Recs'] === '0') {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	protected function fetchStudentCourseList($StudentId) { // Courses attended and enrolled in
		$sql = "SELECT c.`Course_Id`, c.`Course_Number`, a.`App_Desc`, c.`Course_Start_Date`, v.`Venue_Short`, t.`Trainer_Code`
				FROM `Course_Attendee` ca
				JOIN `Course` c ON (ca.`Course_Id` = c.`Course_Id`)
				JOIN `App` a ON (c.`Course_App_Id` = a.`App_Id`)
				JOIN `Venue` v ON (c.`Course_Venue_Id` = v.`Venue_Id`)
				JOIN `Trainer` t ON (c.`Course_Trainer_Id` = t.`Trainer_Id`)
				WHERE ca.`User_Id` = $StudentId
				ORDER BY c.`Course_Start_Date` DESC";
//		echo "<br>$sql<br>";
		return $this->fetchList($sql);
	}
	
	protected function returnCourseStudentList($courseId) {
		$this->fetchCourseStudentList($courseId);
		return $this->courseAttendeeList;
	}
	
	private function fetchCourseStudentList($courseId) {
		$courseAttendeeList = array();
		$courseAttendeeListSQL = "SELECT `User_Id` FROM `Course_Attendee` WHERE `Course_Id` = $courseId";
		$query = $this->query($courseAttendeeListSQL);
		while ($row = mysqli_fetch_array($query)) {
			$courseAttendeeList[$row[0]] = $this->StudentList[$row[0]];
		}
		$this->courseAttendeeList = $courseAttendeeList;
	}
	
}

?>
