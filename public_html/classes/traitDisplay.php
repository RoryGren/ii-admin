<?php

trait traitDisplay {
	
	private function getSelect($selId, $listArray, $selectedItemId, $valuePosition) { // valuePosition = position of value in array
//								'App_Id', $this->AppList, 'zzzz', 0
//		echo "SelectedItemId: $selectedItemId<br>";
//		print_r($listArray);
//		echo "<br>";
//	[298] => Array ( [0] => 298 [Course_Id] => 298 [1] => ReticMaster Core - 2019-03-03 - Hillcrest, KZN [Course] => ReticMaster Core - 2019-03-03 - Hillcrest, KZN [2] => 1 [Course_App_Id] => 1
		$selTop = "<select id='$selId' class='form-control' onchange='doChange(this.id, this.value);'>";
		$selFooter = "</select>";
		$selBody = "<option value='zzzz'";
		if ($selectedItemId === '' || $selectedItemId === 'zzzz') {
			$selBody .= " SELECTED";
		}
		$selBody .= ">==== Select ====</option>";
		if ($selId !== 'Course_Status') {
			foreach ($listArray as $key => $value) {
				$ListId = $value[0];
				$selBody .= "<option value='" . $value[0] . "'";
				if ($selectedItemId == $value[0]) {
					$selBody .= " SELECTED";
				}
				if (key_exists('Course_App_Id', $value)) {
					$selBody .= " data-appId='$value[2]'";
				}
				$selBody .= ">";
				$selBody .= $value[$valuePosition];
				$selBody .= "</option>";
			}
		}
		else {
			foreach ($listArray as $key => $value) {
				$selBody .= "<option value='$key'";
				if ($selectedItemId == $key) {
					$selBody .= " SELECTED";
				}
				$selBody .= ">$value</option>";
			}
		}
		if ($selId !== "Course_Status" && $selId !== 'App_Id') {
			$selBody .= "<option value='createNew'> === Create New === </option>";
		}
		return $selTop . $selBody . $selFooter;
	}
}
?>
