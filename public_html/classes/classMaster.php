<?php

/**
 * Description of classMasters
 *
 * @author rory
 * @displayFields() displays empty fields for adding new master
 */
class classMaster extends mysqli {

	private $FieldList;
	private $CodeList;
	private $CodeField;
	private $tableName;

	public function __construct($table) {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->getFields($table);
		$this->displayFields();
	}
	
	private function getFields($table) {
		$this->tableName = $table;
		$this->FieldList = array();
		$sql = "DESC `$table`";
		$result = $this->query($sql);
		$fields = mysqli_fetch_all($result);
		foreach ($fields as $key => $value) {
			if ($key > 0) {
				if ($key === 1) {
					$this->CodeField = $value[0];
				}
				array_push($this->FieldList, $value[0]);
			}
		}
	}
	
	private function displayFields() { 
	// use defined global TODAY
		foreach ($this->FieldList as $key => $fieldName) {
			if (!strpos($fieldName, 'Create') && !strpos($fieldName, 'View')) {
				$id = substr($fieldName, strlen($this->tableName)+1);
				$field = 'New ' . str_replace('_', ' ', $fieldName);
				echo "<div class='form-group'>";
				echo "<label for='$id'>$field</label>";
				echo "<input type='text' class='form-control' id='$id'>";
				echo "</div>";
			}
		}
		
	}
}

?>
