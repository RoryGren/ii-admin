<!--Unlocks-->
<?php
// Comment out for production
//	ini_set('display_startup_errors', 1);
//	ini_set('display_errors', 1);
//	error_reporting(-1);
//=================================
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "/classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
?>
	<h4>Unlocks</h4>
	<div id="unlockList" class="container-fluid">
		<p>Enter Serial Number:&nbsp;&nbsp;
			<input type="text" id="txtSearch" class="searchText" size="70" >
			&nbsp;&nbsp;
			<button class="btn btn-info" type="button" id="btnQuery">Query</button>
		</p>
	</div>
<div id="divResult"></div>
<script src="scripts/admin.min.js" type="text/javascript"></script>
<script src="scripts/license.min.js" type="text/javascript"></script>
<script>
	// 4608030A-D65C-541D-6FD8-200D-FA5D-D551-A3FF-A100-14.1.1492
	$(".searchText").keyup(function(e) {
		var keycode = (e.keyCode ? e.keyCode : e.which);
		if (keycode === 13) $("#btnQuery").click();
	});
	$("#btnQuery").click(function() {
		var searchString = $("#txtSearch").val();
		decodeSearchString(searchString);
	});

</script>

<?php
    }
    else {
        $_SESSION['LoggedIn'] = False;
        $_SESSION['loggeduser'] = ''; //$User;
        echo '<META HTTP-EQUIV="refresh" CONTENT="3; URL=index.php">';
    };
    
?>
