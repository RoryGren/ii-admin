<?php
	if (!$_SESSION) {session_start();}
//===============================
	echo "<p>licUtils.php</p>";
//===============================
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . "classLogin.php";
			include_once CLASSES . "classLic.php";
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$searchString = filter_input(INPUT_GET, 'searchString', FILTER_SANITIZE_STRIPPED);
			$info = new classLic($User, $searchString);
?>

<?php 

	}
	else {
		header('location: index.php');
	}
?>

