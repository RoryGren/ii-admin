<?php
// ======= Comment these out for production =====
//	ini_set('display_startup_errors', 1);
//	ini_set('display_errors', 1);
//	error_reporting(-1);
// ==============================================
	define('WEB_ROOT', dirname(__FILE__));
	define('ROOT', dirname(WEB_ROOT));
	define('HEAD', WEB_ROOT . '/includes/head.php');
	define('ROOT_INC', ROOT . '/includes/');
	define('WEB_INC', WEB_ROOT . '/includes/');
	define('CLASSES', WEB_ROOT . '/classes/');
	define('SCRIPTS', WEB_ROOT . '/scripts/');
?>
