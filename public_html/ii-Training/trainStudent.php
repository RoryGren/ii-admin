<?php
/*	
 *	Displays complete filterable (JQuery) list of students
 *	New Students MUST have a course, so can only be added from Course page. 
 */
	if (!isset($_SESSION)) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Courses = new classCourseDisplay($User[0]);
			include CLASSES  . "/classStudentDisplay.php";
			$Display = new classStudentDisplay($User[0]);
			$StudentList = $Display->getStudentList();
?>
<script>
//	===== Clear form_modified for listeners =====
	var form_modified = false;

	$(document).ready(function(){
		sessionStorage.setItem('sender','Student');
		sessionStorage.removeItem('senderParent');

//	===== Setup Filter Headers for course list =====
		$('.filterable .btn-filter').click(function(){
			var $panel = $(this).parents('.filterable'),
			$filters = $panel.find('.filters input'),
			$tbody = $panel.find('.table tbody');
			if ($filters.prop('disabled') == true) {
				$filters.prop('disabled', false);
				$filters.first().focus();
			} else {
				$filters.val('').prop('disabled', true);
				$tbody.find('.no-result').remove();
				$tbody.find('tr').show();
			}
		});

		$('.filterable .filters input').keyup(function(e){
			/* Ignore tab key */
			var code = e.keyCode || e.which;
			if (code == '9') return;
			/* Useful DOM data and selectors */
			var $input = $(this),
			inputContent = $input.val().toLowerCase(),
			$panel = $input.parents('.filterable'),
			column = $panel.find('.filters th').index($input.parents('th')),
			$table = $panel.find('.table'),
			$rows = $table.find('tbody tr');
			/* Dirtiest filter function ever ;) */
			var $filteredRows = $rows.filter(function(){
				var value = $(this).find('td').eq(column).text().toLowerCase();
				return value.indexOf(inputContent) === -1;
			});
			/* Clean previous no-result if exist */
			$table.find('tbody .no-result').remove();
			/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
			$rows.show();
			$filteredRows.hide();
			$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
			/* Prepend no-result row if all rows are filtered */
			if ($filteredRows.length === $rows.length) {
				$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
			}
		});

//	===== Setup listener for form_modified for keypresses on input boxes =====
		$(document).on('change keypress', 'input', function() {
			form_modified = true;
//			if ($('#Course_Start_Date').val() && $('#Course_End_Date').val()) {
//				if ($('#Course_Number').val() === "New") {
//					var startDate = $('#Course_Start_Date').val();
//					getNewCourseNo(startDate);
//				};
//			};
		});

//	===== Setup listener for row click on tables =====
		$("table tbody tr").on('click', function() {
			sessionStorage.setItem('senderParent', 'Student');
			sessionStorage.removeItem('courseId');
			var rowId  = this.id;
			showDetail(rowId);
		});
		
	});
</script>
<div class="panel panel-dark filterable">
	<div class="panel-heading"> <!-- Filter button and Filter Headers -->
		<h4>Student List   <span id="RowCount" class="small"></span></h4>
		<div>
			<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
			<!--<button class="btn btn-default btn-xs btn-danger" onclick="addNew('Student');">New Student</button>-->
		</div>
	</div>

	<div id="athList" class="container-fluid"> <!-- Filterable table -->
	<?php
		$Display->tableHeader($StudentList[1]);
		$Display->tableBody($StudentList);
	?>
	</div>
	
</div>

<?php 
	}
	else {
		header('location: ../index.php');
	}
?>