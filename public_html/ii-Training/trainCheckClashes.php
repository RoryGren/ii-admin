<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Check      = new classCourseDisplay($User[0]);
			$ClashType  = filter_input(INPUT_POST, 'clashType');
			$ClashID    = filter_input(INPUT_POST, 'selectValue');
			$ClashStart = filter_input(INPUT_POST, 'startDate');
			$ClashEnd   = filter_input(INPUT_POST, 'endDate');
			return $Check->clash($ClashType, $ClashID, $ClashStart, $ClashEnd);
	}
	else {
		header('location: index.php');
	}
?>
