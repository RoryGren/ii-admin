<!--Displays complete filterable list of courses-->
<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Display = new classCourseDisplay($User[0]);
			$CourseList = $Display->getCourseList();
?>
<script>
//	===== Clear form_modified for listeners =====
	var form_modified = false;
//	sessionStorage.removeItem('senderParent');
	sessionStorage.setItem('senderParent', 'Course');

	$(document).ready(function(){
		sessionStorage.setItem('sender', 'Course');
		sessionStorage.removeItem('selectedCourseId');
//	===== Setup Filter Headers for course list =====
		$('.filterable .btn-filter').click(function(){
			var $panel = $(this).parents('.filterable'),
			$filters = $panel.find('.filters input'),
			$tbody = $panel.find('.table tbody');
			if ($filters.prop('disabled') == true) {
				$filters.prop('disabled', false);
				$filters.first().focus();
			} else {
				$filters.val('').prop('disabled', true);
				$tbody.find('.no-result').remove();
				$tbody.find('tr').show();
			}
		});

		$('.filterable .filters input').keyup(function(e){
			/* Ignore tab key */
			var code = e.keyCode || e.which;
			if (code == '9') return;
			/* Useful DOM data and selectors */
			var $input = $(this),
			inputContent = $input.val().toLowerCase(),
			$panel = $input.parents('.filterable'),
			column = $panel.find('.filters th').index($input.parents('th')),
			$table = $panel.find('.table'),
			$rows = $table.find('tbody tr');
			/* Dirtiest filter function ever ;) */
			var $filteredRows = $rows.filter(function(){
				var value = $(this).find('td').eq(column).text().toLowerCase();
				return value.indexOf(inputContent) === -1;
			});
			/* Clean previous no-result if exist */
			$table.find('tbody .no-result').remove();
			/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
			$rows.show();
			$filteredRows.hide();
			$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
			/* Prepend no-result row if all rows are filtered */
			if ($filteredRows.length === $rows.length) {
				$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
			}
		});

//	===== Setup listener for form_modified for Select changes =====
		$(document).on('change', 'select', function() {
			form_modified = true;
			var selectId = this.id;
			var selectValue = $('#'+selectId).val();
			checkClashes(selectId, selectValue);
		}); // take care of select tags

//	===== Setup listener for form_modified for keypresses on input boxes =====
		$(document).on('change keypress', 'input', function() {
			form_modified = true;
			if ($('#Course_Start_Date').val() && $('#Course_End_Date').val()) {
				if ($('#Course_Number').val() === "New") {
					var startDate = $('#Course_Start_Date').val();
					getNewCourseNo(startDate);
				};
			};
		});

//	===== Setup listener for modal button clicks =====
//		$('.modal .btn').click(function(){
//			doModalClick(this.id);
//		});

//	===== Setup listener for row click on tables =====
		$("table tbody tr").on('click', function() {
			var rowId  = this.id;
			sessionStorage.setItem('sender', 'Course');
			sessionStorage.setItem('senderParent', 'Course');
			sessionStorage.setItem('selectedCourse', rowId);
			sessionStorage.setItem('selectedCourseId', rowId);
			sessionStorage.setItem('courseId', rowId);
//			alert(sessionStorage.getItem('selectedCourseId'));
			showDetail(rowId);
		});

//	=====================================================
//	===== Setup right click context menu for tables =====
//	=====================================================
		(function ($, window) {
		    var menus = {};
			$.fn.contextMenu = function (settings) {
				var $menu = $(settings.menuSelector);
				$menu.data("menuSelector", settings.menuSelector);
				if ($menu.length === 0) return;

				menus[settings.menuSelector] = {$menu: $menu, settings: settings};

				//===== Make sure menu closes on any click
				$(document).click(function (e) {
					hideAll();
				});
				$(document).on("contextmenu", function (e) {
					var $ul = $(e.target).closest("ul");
					if ($ul.length === 0 || !$ul.data("menuSelector")) {
						hideAll();
					}
				});

				//===== Open context menu
				(function(element, menuSelector){
					element.on("contextmenu", function (e) {
						//===== Return native menu if pressing control
						if (e.ctrlKey) return;

						hideAll();
						
						var menu = getMenu(menuSelector);

						//open menu
						menu.$menu
						.data("invokedOn", $(e.target))
						.show()
						.css({
							position: "absolute",
							left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
							top: getMenuPosition(e.clientY, 'height', 'scrollTop')
						})
						.off('click')
						.on('click', 'a', function (e) {
							menu.$menu.hide();

							var $invokedOn = menu.$menu.data("invokedOn");
							var $selectedMenu = $(e.target);

							callOnMenuHide(menu);
							menu.settings.menuSelected.call(this, $invokedOn, $selectedMenu);
						});

						callOnMenuShow(menu);
						return false;
					});
				})($(this), settings.menuSelector);

				function getMenu(menuSelector) {
					var menu = null;
					$.each( menus, function( i_menuSelector, i_menu ){
						if (i_menuSelector == menuSelector) {
							menu = i_menu
							return false;
						}
					});
					return menu;
				}
				function hideAll() {
					$.each( menus, function( menuSelector, menu ){
						menu.$menu.hide();
						callOnMenuHide(menu);
					});
				}

				function callOnMenuShow(menu) {
					var $invokedOn = menu.$menu.data("invokedOn");
					if ($invokedOn && menu.settings.onMenuShow) {
						menu.settings.onMenuShow.call(this, $invokedOn);
					}
				}
				function callOnMenuHide(menu) {
					var $invokedOn = menu.$menu.data("invokedOn");
					menu.$menu.data("invokedOn", null);
					if ($invokedOn && menu.settings.onMenuHide) {
						menu.settings.onMenuHide.call(this, $invokedOn);
					}
				}

				function getMenuPosition(mouse, direction, scrollDir) {
					var win = $(window)[direction](),
						scroll = $(window)[scrollDir](),
						menu = $(settings.menuSelector)[direction](),
						position = mouse + scroll - menu + 10;

					// opening menu would pass the side of the page
					if (mouse + menu > win && menu < mouse) {
						position -= menu;
					}

					return position;
				}    
						return this;
			};
		})(jQuery, window);

		$("#tableList tbody tr").contextMenu({
			menuSelector: "#contextMenu",
			menuSelected: function ($invokedOn, $selectedMenu) {
				sessionStorage.setItem('courseId', $invokedOn.parent('tr').attr('id'));
				sessionStorage.setItem('courseNumber', $invokedOn.siblings(":first").text());
				sessionStorage.setItem('sender', 'Student');
				sessionStorage.setItem('senderParent', 'Course');
				eval($selectedMenu.attr("value"));
//				var msg = "MENU 1\nYou selected the menu item '" + $selectedMenu.text() + 
//					"' (" + $selectedMenu.attr("value") + ") " +
//					" on the value '" + courseId + "' for Course No: "+courseNo;
			},
			onMenuShow: function($invokedOn) {
				var tr = $invokedOn.closest("tr");
				$(tr).addClass("warning");
			},
			onMenuHide: function($invokedOn) {
				var tr = $invokedOn.closest("tr");
				$(tr).removeClass("warning");
			}
		});


		$("#tableList tbody td.username").contextMenu({
			menuSelector: "#contextMenuUsername",
			menuSelected: function ($invokedOn, $selectedMenu) {
				var msg = "MENU 2\nYou selected the menu item '" + $selectedMenu.text() + 
					"' (" + $selectedMenu.attr("value") + ") " +
					" on the value '" + $invokedOn.text() + "'";
//				alert('Umm '+msg);
			},
			onMenuShow: function($invokedOn) {
				$invokedOn.addClass("success");
			},
			onMenuHide: function($invokedOn) {
				$invokedOn.removeClass("success");
			}
		});
		
//	=====================================================
//	=====================================================
		
	});
</script>
<div class="panel panel-dark filterable">
	<div class="panel-heading"> <!-- Filter button and Filter Headers -->
		<h4>Course List   <span id="RowCount" class="small"></span></h4>
		<div>
			<button id="btnCourseFilter" class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
			<span class="notice">Newest Courses at the top</span>&nbsp;&nbsp;&nbsp;
			<button class="btn btn-default btn-xs btn-danger" onclick="addNew('Course');">New Course</button>
		</div>
	</div>

	<div id="athList" class="container-fluid"> <!-- Filterable table -->
	<?php
		$Display->tableHeader($CourseList[1]);
		$Display->tableBody($CourseList);
	?>
	</div>
</div>
<ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none" >
    <!--<li><a tabindex="-1" href="#" value="menu1:addStudent">Add Students to Course</a></li>-->
    <li><a tabindex="-1" href="#" value="addNew()">Add Students to Course</a></li>
    <!--<li class="divider"></li>-->
    <li><a tabindex="-1" href="#" value="listExistingStudents()">Quick-List: Existing Students</a></li>
</ul>
<!--<ul id="contextMenuUsername" class="dropdown-menu" role="menu" style="display:none" >
    <li><a tabindex="-1" href="#" value="menu2:action-1">User action</a></li>
    <li><a tabindex="-1" href="#" value="menu2:action-2">Another user action</a></li>
</ul>-->


<?php 
	}
	else {
	header('location: index.php');
	}
?>
