<?php
//  Displays one student either from double click on grid or by clicking New and entering exiting ID
//	echo "file: trainStudentDetail.php<br>";
	if (empty($_SESSION)) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$User = $_SESSION['UID'];
			$studentId = filter_input(INPUT_GET, 'rowId', FILTER_SANITIZE_NUMBER_INT);
			$selectedCourseId = filter_input(INPUT_GET, 'selectedCourseId', FILTER_SANITIZE_NUMBER_INT);
			include CLASSES  . "/classStudentDisplay.php";
			$Display = new classStudentDisplay($User[0]);
//			$StudentList = $Display->getStudentList();
			$Display->showStudentDetail($studentId, $selectedCourseId);
?>

<?php
		}
		else {
			header('location: ../index.php');
		}
?>
