<?php
	if (!$_SESSION) {session_start();}
//	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Display = new classCourseDisplay($User);
			$VenueList = $Display->returnMasters('VenueListDetail');
?>
	<script type="text/javascript">
		$(document).ready(function() {
//			$("table tbody tr").on('click', function() {
//				var sender = $('div.active').attr('id');
//				var rowId  = this.id;
//				showDetail(sender, rowId);
//			})
			$('.filterable .btn-filter').click(function(){
				var $panel = $(this).parents('.filterable'),
				$filters = $panel.find('.filters input'),
				$tbody = $panel.find('.table tbody');
				if ($filters.prop('disabled') == true) {
					$filters.prop('disabled', false);
					$filters.first().focus();
				} else {
					$filters.val('').prop('disabled', true);
					$tbody.find('.no-result').remove();
					$tbody.find('tr').show();
				}
			});

			$('.filterable .filters input').keyup(function(e){
				/* Ignore tab key */
				var code = e.keyCode || e.which;
				if (code == '9') return;
				/* Useful DOM data and selectors */
				var $input = $(this),
				inputContent = $input.val().toLowerCase(),
				$panel = $input.parents('.filterable'),
				column = $panel.find('.filters th').index($input.parents('th')),
				$table = $panel.find('.table'),
				$rows = $table.find('tbody tr');
				/* Dirtiest filter function ever ;) */
				var $filteredRows = $rows.filter(function(){
					var value = $(this).find('td').eq(column).text().toLowerCase();
					return value.indexOf(inputContent) === -1;
				});
				/* Clean previous no-result if exist */
				$table.find('tbody .no-result').remove();
				/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
				$rows.show();
				$filteredRows.hide();
				$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
				/* Prepend no-result row if all rows are filtered */
				if ($filteredRows.length === $rows.length) {
					$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
				}

			});
			$('#planButtons').addClass('hidden');
		});
		$("table tbody tr").on('click', function() {
			var MasterCategory = $('div.active').attr('id');
			var rowId  = this.id;
			sessionStorage.setItem('sender', 'Master');
			sessionStorage.setItem('master', MasterCategory);
			showDetail(rowId);
		});
	</script>
<div class="masters"> <!-- ----- Tabs ------->
	<h4 title="Maintain Master Lists" >Master Records</h4>
	<ul class="nav nav-tabs">
		<li class="active">	<a data-toggle="tab" href="#Trainer" >Trainer</a></li>
		<li>				<a data-toggle="tab" href="#App">App</a></li>
		<li>				<a data-toggle="tab" href="#Venue"  >Venue</a></li>
	</ul>

	<div class="tab-content"> <!-- ----- Tab Content ------->
<!-- Trainers -->
		<div id="Trainer" class="tab-pane fade in active">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Trainer&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('Trainer', 'New')">Add</button>
						</h4>
					</div>

				</div>
				<div id="divTrainer">
					<?php // $Display->masterList('Trainer'); ?>
				</div>
			</div>
	   </div>
<!-- Apps -->
		<div id="App" class="tab-pane fade">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Apps&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('App', 'New')">Add</button>
						</h4>
					</div>
				</div>
				<div id="divApp">
					<?php // $Display->masterList('App'); ?>
				</div>
			</div>
		</div>
<!-- Venues -->
		<div id="Venue" class="tab-pane fade">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Venues&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('Venue', 'New')">Add</button>
						</h4>
					</div>
				</div>
				<div id="divVenue">
					<?php
						$Display->tableHeader($VenueList[0]);
						$Display->tableBody($VenueList);
					?>

				</div>
			</div>
		</div> 
<!-- ----- End Tab Content ------->
	</div>
</div>
<?php 
	}
	else {
	header('location: index.php');
	}
?>
