<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once '../config.php';
			include_once ROOT_INC . "/adminConfig.php";
			include_once CLASSES . '/classCourseDisplay.php';
			include_once CLASSES . '/classStudentDisplay.php';
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			$SelectedCourseId = filter_input(INPUT_GET, 'rowId', FILTER_SANITIZE_NUMBER_INT);
			$Display = new classCourseDisplay($User[0]);
//			$Class   = new classStudentDisplay($User[0]);
			$CourseList = $Display->getCourseList();
//			$CourseClassList = $Class->getClassList($SelectedCourseId);
//			$SelectedCourse = $CourseList[$SelectedCourseId];
			$Display->courseDetail($SelectedCourseId);
?>

<?php 
	}
	else {
	header('location: index.php');
	}
?>