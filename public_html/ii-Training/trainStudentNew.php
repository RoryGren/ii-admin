<!--Displays blank data capture form when new ID is entered--> 
<?php
//echo "file = trainStudentNew.php<br>";
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Courses = new classCourseDisplay($User[0]);
			$CourseList = $Courses->getCourseList();
			// TODO =====> Set active course for new student <=====
//			$data = filter_input(INPUT_GET, 'data');
//			var_dump($data);
//			echo "<br>";
//			print_r($CourseList[298]);
//			echo "<br><br>";
			// =================================================================
			include CLASSES  . "/classStudentDisplay.php";
			$Display = new classStudentDisplay($User[0]);
			$Display->getNewStudentId();
?>


<?php
		}
		else {
			header('location: index.php');
		}
?>
