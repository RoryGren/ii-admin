<?php
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "/adminConfig.php";
			include_once CLASSES . "/classCourseDisplay.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
//			include_once CLASSES  . "/classLogin.php";
			$User = $_SESSION['UID'];
//			$info = filter_input(INPUT_GET, 'info');
			$Display = new classCourseDisplay($User[0]);
//			Array ( [info] => Array ( [0] => Array ( [id] => Course_Id [value] => New ) [1] => Array ( [id] => Course_Number [value] => 1806 ) [2] => Array ( [id] => Course_Start_Date [value] => 2018-09-09 ) [3] => Array ( [id] => Course_End_Date [value] => 2018-09-10 ) [4] => Array ( [id] => App_Id [value] => 5 ) [5] => Array ( [id] => Trainer_Id [value] => 4 ) [6] => Array ( [id] => Venue_Id [value] => 0 ) [7] => Array ( [id] => Course_Status [value] => 1 ) ) )
			$job  = filter_input(INPUT_GET, 'job');
			$info = json_decode(filter_input(INPUT_GET, 'info'));
			$Cid = $info[0]->value;
//			echo "trainCourseUpdate.php<br>";
//			echo "Info: ";
//			print_r($info);
//			echo "<br>";
			$Display->updateCourse($Cid, $info, $User[0], $job);
		}
		else {
			header('location: index.php');
		}
?>
