<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . '/classCourseDisplay.php';
			$Display = new classCourseDisplay($User[0]);
			
			// not getting start date...
			$startDate = filter_input(INPUT_POST, "startDate");
//			echo "trainCourseNumberNew: $startDate<br>";
			$Display->newCourseNumber($startDate);
?>


<?php
	}
	else {
	header('location: index.php');
	}
?>