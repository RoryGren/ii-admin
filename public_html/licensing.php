<?php
//	header("Set-Cookie: HttpOnly;Secure;SameSite=Strict");
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . "classLogin.php";
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
?>
<!DOCTYPE html>
<html>
    <head>
<?php 
	include HEAD; 
?>
	</head>
	<body>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="padding-top"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-sm-2 leftNav">
					<h4 class="text-center">Licensing</h4>
					<button id="licUnlocks"		class="btn btn-dark btn-block active"	title="Search by Company">Unlocks</button>
					<button id="crmContact"		class="btn btn-dark btn-block"			title="Search by Contact">Something / Contacts</button>
					<button id="crmWebContact"	class="btn btn-dark btn-block"			title="Messages from the Website">and something else...</button>
				</div>
				<div class="col col-sm-10" id="working">
					<?php include 'ii-Licensing/licUnlocks.php' ?>
				</div>
				<!--<div class="col col-sm-1"></div>-->
			</div>
		</div>
		<?php
			include WEB_INC . 'modal.php';
		?>
		<script>
			$(document).ready(function() {
				$('#navLicenses').siblings('li').removeClass('active');
				$('#navLicenses').addClass('active');
				sessionStorage.setItem('module', 'licensing');
				$('.leftNav>button').on('click', function(event) {
					showFile(event);
				});
			});
		</script>
    </body>
</html>
<?php 

	}
	else {
	header('location: index.php');
	}
?>

