<?php
	if (!isset($_SESSION)) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . 'classLogin.php';
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
?>
<!DOCTYPE html>
<html>
    <head>
<?php 
	include HEAD; 
?>
	</head>
	<body>
		<script>
			$(document).ready(function() {
				$('#navAdmin').siblings('li').removeClass('active');
				$('#navAdmin').addClass('active');
				sessionStorage.setItem('module', 'Admin');
				$('.leftNav>button').on('click', function(event) {
					showFile(event);
				});
			});
		</script>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="padding-top"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-md-2 leftNav">
					<h4 class="text-center">Admin</h4>
					<button id="adminAccess"	class="btn btn-dark btn-block active"	title="Search by Company">Access Control</button>
					<!--<button id="crmContact"		class="btn btn-dark btn-block"			title="Search by Contact">Contacts</button>-->
					<!--<button id="crmWebContact"	class="btn btn-dark btn-block"			title="Messages from the Website">Open Web Requests</button>-->
				</div>
				<div class="col col-md-9" id="working">
					<?php include 'ii-Admin/adminAccess.php' ?>
				</div>
				<div class="col col-md-1"></div>
			</div>
		</div>
    </body>
</html>
<?php 
	}
	else {
	header('location: index.php');
	}
?>

