<?php
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . "classLogin.php";
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
?>
<!DOCTYPE html>
<html>
    <head>
<?php 
	include HEAD; 
?>
	</head>
	<body>
		<script>
			$(document).ready(function() {
				$('#navCRM').siblings('li').removeClass('active');
				$('#navCRM').addClass('active');
				sessionStorage.setItem('module', 'CRM');
				$('.leftNav>button').on('click', function(event) {
					showFile(event);
				});
			});
		</script>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="padding-top"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-sm-2 leftNav">
					<h4 class="text-center">CRM</h4>
					<button id="crmCompany"		class="btn btn-dark btn-block active"	title="Search by Company">Company</button>
					<button id="crmContact"		class="btn btn-dark btn-block"			title="Search by Contact">Users / Contacts</button>
					<button id="crmWebContact"	class="btn btn-dark btn-block"			title="Messages from the Website">Open Website Messages</button>
				</div>
				<div class="col col-sm-10" id="working">
					<?php include 'ii-CRM/crmCompany.php' ?>
				</div>
				<!--<div class="col col-sm-1"></div>-->
			</div>
		</div>
    </body>
</html>
<?php 
	include WEB_INC . 'modal.php';

	}
	else {
	header('location: index.php');
	}
?>

