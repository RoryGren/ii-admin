<?php
// TODO =====> Report-a-Bug | Front-end and Back-end
// TODO =====> Request-a-feature | Front-end and Back-end
// TODO =====> Requests and Bug-fix progress
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			include_once "config.php";
			include_once ROOT     . "/includes/adminConfig.php";
			include_once WEB_ROOT . '/classes/classLogin.php';
?>
<!DOCTYPE html>
<html>
    <head>
<?php 
	include HEAD; 
?>
	</head>
	<body>
		<script>
			$(document).ready(function() {
					$('#navProducts').siblings('li').removeClass('active');
					$('#navProducts').addClass('active');
					sessionStorage.setItem('sender', 'Products');
					$('.menu-left>button').on('click', function() {
						showFile(event);
					});
				});
		</script>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-md-2 menu-left">
					<h4 class="text-center">Products</h4>
					<button id="prodIssueList" class="btn btn-dark btn-block active" onclick="// showFile('trainCourse');" title="Modify existing Issue data">Issue</button>
<!--					<button id="trainStudent"  class="btn btn-dark btn-block" onclick="// showFile('trainStudent');" title="Modify existing Student data">Students</button>
					<button id="trainReport"  class="btn btn-dark btn-block" onclick="// showFile('trainReport');" title="Manual capture">Reporting</button>-->
				</div>
				<div class="col col-md-9" id="working">
					<?php include 'ii-Products/prodIssueList.php' ?>
				</div>
				<div class="col col-md-1"></div>
			</div>
		</div>
    </body>
</html>
<?php 
	}
	else {
	header('location: index.php');
	}
?>