
function showFile(e) {
	console.log('showFile()');
	var newFile = e.target.id;
	console.log("newFile: "+newFile);
	var btn = '#'+newFile;
	console.log("btn: "+btn);
	var module = sessionStorage.getItem('module');
	console.log("module: "+module);
	var folder = 'ii-'+module;
	console.log("folder: "+folder);
	var path   = folder+'/'+newFile+'.php';
	console.log("path: "+path);
	$(btn).siblings('button').removeClass('active');
	$(btn).addClass('active');
	$('#working').empty();
	$('#working').load(path);
}

function showDetail(rowId) {
// ====> display detail for selected table row in modal or empty for new student
	var url       = '',
		module    = sessionStorage.getItem('module'),
		type      = sessionStorage.getItem('sender'),
		parent    = sessionStorage.getItem('senderParent'),
		currentId = sessionStorage.getItem('currentStudentId'),
		selectedCourseId = sessionStorage.getItem('courseId'),
		courseNo  = '',
		selectedMaster;
//	console.log("showDetail: Module: '"+module+"' :Type: '"+type+"' :: "+rowId);
		alert('Sender: '+type+', Parent: '+parent);
	if (type === 'Course') {
		url = 'ii-Training/trainCourseDetail.php';
		title = 'Course Detail';
//		alert(sessionStorage.getItem('courseId')+' :: '+selectedCourseId);
	}
	else if (type === 'Student') {
		url = 'ii-Training/trainStudentDetail.php';
		title = 'Student Detail';
		if (selectedCourseId !== 'zzzz' && parent === 'Course') {
			courseNo = " - Course "+sessionStorage.getItem('courseNumber');
		}
	}
	else if (type === 'Company') {
		url = 'ii-CRM/crmCompanyDetail.php';
		title = 'Company Detail';
	}
	else if (type === 'Contact') {
		url = 'ii-CRM/crmContactDetail.php';
		title = 'Contact Detail';
		console.log('Contact: '+rowId);
	}
	else if (type === 'Master') {
		url = 'ii-Training/trainMasterDetail.php';
		title = 'Venue Detail';
		selectedMaster = $('.nav-tabs>li.active>a').text();
		console.log('Master: '+selectedMaster);
	}
	$.ajax({
		url:url,
		type: 'GET',
		data:{
			rowId: rowId,
			selectedCourseId: selectedCourseId
		},
		success: function(data) {
			$('#modal-title').html(title);
			$('#modal-title').append(courseNo);
			$('#modal-body').html(data);
			$('.modal').modal('show');
			$('.modal-footer>button').show();
			$('#btnDelete').removeAttr('disabled');

			if (rowId.indexOf('new') >= 0) {
				$('.modal-footer #btnDelete').hide();
				$('#User_Id_Number').val(currentId);
				$('#checkIdButton').parent('td').parent('tr').addClass('hidden');
				$('#User_Last').focus();
				sessionStorage.removeItem('currentStudentId');
			}
			else {
				$('#Course_Id').parent('td').parent('tr').addClass('hidden');
			}
			if (type === 'Student') {
				$('#btnDelete').hide();
			}
			if ((type === 'Course') && ($('#learnerCount').text() > 0)) {$('#btnDelete').attr('disabled', 'disabled');}
			$('#modal-body').children('form').append("<input type='hidden' id='senderParent' value='"+parent+"'>");
		}
	})
}

function showCompanyDetail(rowId) {
	console.log("function showCompanyDetail()");
	console.log("Branch_Id: "+rowId);
	url = 'ii-CRM/crmCompanyDetail.php';
	title = 'Company Detail';
	$.ajax({
		url:url,
		type: 'GET',
		data:{
			rowId: rowId
		},
		success: function(data) {
			$('#divDetails').html(data);
		}
	});
}

function addNew(sender) { 
// display empty detail form in modal with relevant fields
	
	console.log("addNew("+sender+")");
	var title, data;
	var courseNo = '';
	if ( sender != undefined) {
		sessionStorage.setItem('sender', sender);
		console.log(sender);
	}
	var type = sessionStorage.getItem('sender');
	console.log("Type: "+type);
	if (type === 'Course') {
		url = 'ii-Training/trainCourseNew.php';
		title = 'New Course';
	}
	else if (type === 'Products') {
		url = 'ii-Products/trainCourseNew.php';
		title = 'New Course';
	}
	else if (type === 'Company') {
		url = 'ii-CRM/companyNew.php';
		title = 'New Company';
	}
	else if (type === 'Student') {
		if (sessionStorage.getItem('senderParent') === 'Course') {
			var selectedCourseId = sessionStorage.getItem('courseId'),
				courseNo = " - Course "+sessionStorage.getItem('courseNumber');
			data = {
				selectedCourseId: sessionStorage.getItem('courseId')
			};
//			alert('addNew() :: Student :: sender: '+sender+' course: '+selectedCourseId);
		}
		url = 'ii-Training/trainStudentNew.php';
		title = 'New Student';
	}
	$('#modal-title').html(title);
	$('#modal-title').append(courseNo);
	$('#modal-body').load(url, data);
	$('.modal-footer #btnDelete').hide();
	$('.modal').modal('show');
	if (type === 'Student') {
		$('.modal-footer>button').hide();
	}
}

function doModalClick(btnId) {
//	===== Detects button clicked on modal and calls relevant script =====
	console.log("doModalClick("+btnId+")");
	var sender = sessionStorage.getItem('sender'),
		parent = sessionStorage.getItem('senderParent'),
		activeFormId = $('form').prop('id');
	var confirmDeleteMessage = 'Permanently delete '+sender+' ';
	var noDeleteMessage = '';
	var canDelete = true;
	var fieldList=[];
	var idField; 
	var idVal;
	var fieldCount = 0;
	var emptyFieldCount = 0;
	var emptyFieldList  = '';
	var job = '';
	$('#btnCancel').text('Cancel');
//	alert('sender: '+sender);
// ===== Set default values for sender =====
	if (sender === 'Course') {
		// ===== Course specific values =====
		confirmDeleteMessage = confirmDeleteMessage+$('#Course_Number').val()+'?';
		url = 'ii-Training/trainCourseUpdate.php';
		idField = 'Course_Id';
		idVal = $('#Course_Id').val();
		var learnerCount = $('#learnerCount').text();
		if (learnerCount > 0) {
			canDelete = false;
			noDeleteMessage = 'There are '+$('#learnerCount').text()+' students registered on this course.\r\nPlease clear them first.';
		}
	}
	if (sender === 'Student') {
		// ===== Student specific values =====
		confirmDeleteMessage = confirmDeleteMessage+$('#Student_Id').val()+'?';
		noDeleteMessage = "Delete cancelled.";
		url = 'ii-Training/trainUpdate.php';
		idField = 'User_Id_Key';
		if ($('#Student_Id').val() === '') {$('#Student_Id').val('new');}
		idVal = $('#Student_Id').val();
	}
//	===== Build parameters dependant on Button selected =====
	switch (btnId) {
		case 'btnDelete':
			if (canDelete && confirm(confirmDeleteMessage)) {
				job = 'Delete';
				fieldList.push({id:idField, value:idVal});
				console.log('InSwitch: Delete '+idVal);
			}	
			else {
				alert(noDeleteMessage);
				job = 'Cancel';
				console.log('InSwitch: Delete canceled '+idVal);
			}
			break;
		case 'btnSave':
			if (form_modified) {
				job = 'Modify';
				formId = activeFormId;
				$("#"+formId+" input, select").each(function() { 
					// ===== Collect form input data =====
					fieldCount ++;
					if ($(this).val() == '' || $(this).val() === 'zzzz') { // check if empty
						$(this).addClass('border-red');
						emptyFieldCount ++;
						emptyFieldList += $(this).prop('id') + ', ';
					}
					else { // has content
						$(this).removeClass('border-red');
						fieldList.push({id:$(this).prop('id'), value:$(this).val()});
					}
				});
			}
			else {
				job = 'Cancel';
			}
			break;
		default:
			job = 'Cancel';
	}
	console.log('Sender: '+sender+', Parent: '+parent+', job:: '+job);
	if (job !== 'Cancel') {
//	===== Save or Delete detected => send request to php =====
		fieldList = JSON.stringify(fieldList);
		console.log('Sender: '+sender+', Parent: '+parent+', job:: '+job);
		$.ajax({
			url: url,
			data: {
				info: fieldList,
				job: job,
				sender: sender
			},
			success: function() {
//				$('#working').html(data);			 // =====> DEBUG Comment this for production
				form_modified = false;
				if (sender === 'Student' && parent === 'Course' && job === 'Modify') {
					// ===== Allow repeat checkId capture form instead of reloading company list
					var courseNo = sessionStorage.getItem('courseNumber');
					url = 'ii-Training/trainStudentNew.php';
					data = {
						selectedCourseId: sessionStorage.getItem('courseId')
					};
					title = 'New Student - Course '+courseNo;
					$('#modal-title').html(title);
					$('#modal-body').load(url, data);
					$('.modal-footer>button').hide();
					$('#btnCancel').toggle();
					$('#btnCancel').text('Close');
				}
				else {
					// ===== Hide modal and reload sender page =================
					$('.modal').modal('hide');			 // =====> DEBUG Uncomment this for production
					$('#modal-body').html('');
					$('.leftNav>button.active').click(); // =====> DEBUG Uncomment this for production
					form_modified = false;
				}
								 
			}
		});
	}
	else {
		// ===== Cancel clicked, reload sender page ==========================
		$('.leftNav>button.active').click();
	}
}

function doChange(selId, selValue) { 
// display form to capture details for new master from select element
	var params = {selId:selId}
	console.log("doChange("+selId+", "+selValue+")");
	if (selValue === 'createNew') {
		var url = 'includes/addMaster.php';
//		console.log(selId+", "+selValue);
		$('#'+selId).parent('td').load(url,params);
		$('.modal-footer .btn').attr('disabled', 'disabled');
	}
	if (selId === 'App_Id') { // filter available courses based on selValue (App_Id)
		$("#Course_Id").children('option').show();
		$("#Course_Id>option[data-appId!='"+selValue+"']").hide();
		$("#Course_Id>option[value='zzzz']").show();
	}
	if (selId === 'Course_Status') { // filter available courses based on selValue (App_Id)??
		// Do we need to do anything here? Just changing status. Will be updated with Save button click
	}
}

function updateWebContacts(chId) {
	var rowId = chId.substring(7,9);
	var isChecked = $('#'+chId).prop('checked');

	console.log('updateWebContacts('+chId+')');
	console.log(rowId);
	console.log('isChecked: '+isChecked);
	$.ajax({
		url:'ii-CRM/crmWebContactUpdate.php',
		type: 'POST',
		data: {
			rowId: rowId,
			isChecked: isChecked
		},
		success: function(data) {
			$('#working').html(data);
//			$('#working').load('ii-CRM/crmWebContact.php');
		}
	}).then(function(){
		$('#working').load('ii-CRM/crmWebContact.php');
	});
}

function checkClashes(selectId, selectValue) {
	var sender = sessionStorage.getItem('sender');
	console.log(sender+": Checking Clashes... "+selectId+", "+selectValue);
	if (sender === 'Course') {
		sessionStorage.setItem('anyClashes', 'true'); 
		checkDates(); // Check valid dates entered.
		if (selectId === "Trainer_Id") {
			console.log("Check " + selectId);
			$.post({
				url: 'ii-Training/trainCheckClashes.php',
				data: {
					clashType: selectId,
					selectValue: selectValue,
					startDate: $("#Course_Start_Date").val(),
					endDate: $("#Course_End_Date").val()
				},
				success: function(data) {
					console.log(data);
					if(data !== '0'){
						console.log("Data=0 :: Clash");
						alert("Selected Trainer has a clash");
						$('#btnSave').attr('disabled', 'disabled');
						$('#'+selectId).addClass('border-red');
						$('#'+selectId).focus();
					}
					else {
						$('#btnSave').removeAttr('disabled');
						$('#'+selectId).removeClass('border-red');
						alert("No clashes found for "+selectId);
					}
				}
			})
		}
	}
}

function checkDates() {
	console.log("Checking dates");
	var startDate = new Date($("#Course_Start_Date").val());
	var endDate = new Date($("#Course_End_Date").val());
//==============================================================================
	if (startDate == "Invalid Date") {
		console.log("Invalid Start Date");
		alert("Please enter a valid Start Date");
		resetSelects();
		$('#Course_Start_Date').addClass('border-red');
		$('#Course_Start_Date').focus();
	}
	else {
		$('#Course_Start_Date').removeClass('border-red');
//==============================================================================
		if (endDate == "Invalid Date") {
			console.log("Invalid End Date");
			alert("Please enter a valid End Date");
			resetSelects();
			$('#Course_End_Date').addClass('border-red');
			$('#Course_End_Date').focus();
		}
		else {
			$('#Course_End_Date').removeClass('border-red');
			console.log("Dates OK");
		}
	}
//==============================================================================
}

function getNewCourseNo(startDate) {
	console.log("get new course number! "+startDate);	
	$.ajax({
		url: 'ii-Training/trainCourseNumberNew.php',
		type: 'POST',
		data: {
			startDate: startDate
		},
		success: function(data) {
			$('#Course_Number').val(data);
//			$('#modal-body').html(data);
		}
	});
}

function resetSelects(selectId) {
	if (selectId !== '') {
		$('#'+selectId).val('zzzz');	
	}
	else {
		$('#Trainer_Id').val('zzzz');
		$('#Venue_Id').val('zzzz');
		$('#App_Id').val('zzzz');
	}
}

function checkId(idNumber) {
//	console.log('checkId('+idNumber+')');
	sessionStorage.setItem('currentStudentId', idNumber);
	var selectedCourse = sessionStorage.getItem('courseId');
	$.ajax({ 
	// =========================================================================
	//	ajax calls checkId.php which returns: 
	//	'Enrolled' to return to Check Id form if userId is registered for selected course OR
	//	'new' to allow new data capture, OR
	//	User_Id_Key to retrieve user data for function showDetail()
	// =========================================================================
		url: 'common/checkId.php', 
		data:{
			User_Id_No: idNumber,
			Selected_Course_Id: selectedCourse
		},
		success:function(data) {
			console.log('data: '+data);
			if (data.indexOf('Enrolled') > 0) { // id Number exists and user already enrolled in the course 
				alert('User is already enrolled for selected course.');
			}
			else {
				if (data.indexOf('new') >= 0) { // idNumber does not exist in database => enable data capture
					sessionStorage.setItem('newStudent', 'true');
				}
				else { // idNumber exists in database => call trainStudentDetail.php to populate fields
					sessionStorage.setItem('sender', 'Student');
					sessionStorage.setItem('newStudent', 'false');
				}
				// ===== Show Student Detail Form =====
				showDetail(data);
			}
		}
	});
}

function doCert() {
	$('#Att_Cert').val($('#User_First').val()+' '+$('#User_Last').val());
	$('#User_Nick').val($('#User_First').val());
}

function openDetail_Never_Called(rowId) { 
	// this function is never called..... uses showDetail() instead.
	alert('openDetail is used!');
// ====> display detail for selected table row in modal or empty for new student
	var url       = '',
		module    = sessionStorage.getItem('module'),
		type      = sessionStorage.getItem('sender'),
		currentId = sessionStorage.getItem('currentStudentId'),
		selectedCourseId = sessionStorage.getItem('courseId'),
		selectedMaster;
	console.log("showDetail: Module: '"+module+"' :Type: '"+type+"' :: "+rowId);
	$('.modal-footer>button').show();
	if (type === 'Course') {
		url = 'ii-Training/trainCourseDetail.php';
		title = 'Course Detail';
	}
	else if (type === 'Student') {
		url = 'ii-Training/trainStudentDetail.php';
		title = 'Student Detail';
	}
	else if (type === 'Company') {
		url = 'ii-CRM/crmCompanyDetail.php';
		title = 'Company Detail';
//		alert("Company");
	}
	else if (type === 'Master') {
		url = 'ii-Training/trainMasterDetail.php';
		title = 'Venue Detail';
		selectedMaster = $('.nav-tabs>li.active>a').text(); //.attr('href');
		console.log('Master: '+selectedMaster);
//		XXXXX
	}
	$.ajax({
		url:url,
		type: 'GET',
		data:{
			rowId: rowId
		},
		success: function(data) {
			$('#modal-title').html(title);
			$('#modal-body').html(data);
			$('.modal').modal('show');
			if (rowId.indexOf('new') >= 0) {
				$('.modal-footer #btnDelete').hide();
				$('#User_Id_Number').val(currentId);
				$('#checkIdButton').parent('td').parent('tr').addClass('hidden');
				$('#User_Last').focus();
				sessionStorage.removeItem('currentStudentId');
			}
			$('#Course_Id').val(selectedCourseId).change();
		}
	})
}

function listExistingStudents() {
//	alert('Not yet! :: listExistingStudents()');
	var type = sessionStorage.getItem('sender'),
		senderType = sessionStorage.getItem('senderParent'),
		selectedCourseId    = sessionStorage.getItem('courseId'),
//		selectedCourseNo    = sessionStorage.getItem('courseNumber'),
		selectedCourseNo   = $('#'+selectedCourseId).children('td:nth-child(1)').text(),
		selectedStartDate   = $('#'+selectedCourseId).children('td:nth-child(2)').text(),
		selectedEndDate     = $('#'+selectedCourseId).children('td:nth-child(3)').text(),
		selectedCourseDesc  = $('#'+selectedCourseId).children('td:nth-child(4)').text(),
		selectedCourseTrainer = $('#'+selectedCourseId).children('td:nth-child(5)').text(),
		selectedCourseVenue = $('#'+selectedCourseId).children('td:nth-child(6)').text();
//console.log(selectedCourseId+' :: '+selectedCourseNo+' :: '+selectedStartDate+' :: '+selectedEndDate+' :: '+selectedCourseDesc+' :: '+selectedCourseTrainer+' :: '+selectedCourseVenue);
//	var title = 'Class List';
	var title = '<h5><u>Class List</u>: Course '+selectedCourseNo+'<br><br>'+selectedCourseDesc+'</h5><h6>Dates: '+selectedStartDate+' - '+selectedEndDate+'</h6><h6>Venue: '+selectedCourseVenue+'</h6>';
	var courseHeader = '';
//	var courseHeader = '<h6>'+selectedStartDate+' to '+selectedEndDate+'</h6>';
//		courseHeader += '<h6>Class List:</h6>';
	url = 'ii-Training/trainCourseClassList.php';
	$.ajax({
		url: url,
		type: 'GET',
		data: {
			selectedCourseId: selectedCourseId
		},
		success: function(data) {
//			$('#working').html(data);			 // =====> DEBUG Comment this for production
			$('#modal-title').html(title);
			$('#modal-body').html(data);
//			$('#modal-body').prepend(courseHeader);
			$('#btnSave').hide();
			$('#btnDelete').hide();
			$('#btnCancel').text('Close');
			$('.modal').modal('show');
		}
	});
}