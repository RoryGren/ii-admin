
function showAccessDetail(rowId) {
	var module = sessionStorage.getItem('module');
	var path = 'ii-'+module;
	var div = "#userDetail";
	var url = path+'/'+sessionStorage.getItem('sender')+'Detail.php';
//	console.log(url);
	$.ajax({
		url:url,
		data:{rowId: rowId},
		success: function(data) {
			$(div).html(data);
		}
	});
}

function doAdminAccessUpdate(action) {
	var	newInfo = {};
	var	rowId = $('#rowId').val();
	if ($('#'+action).hasClass('btnAccessControl')) {
		switch(action) {
			case 'btnSave':
				newInfo = {
					task: action,
					Per_Name:  $('#Name').val(),
					Per_Desc:  $('#Desc').val(),
					Per_Short: $('#Short').val(),
					Per_Level: $('#Level').val(),
				};
				break;
			//==================================================================
			case 'btnReset':
				var newPwd = window.prompt("Enter New Password:","NewPassword123*"); // TODO =====> Clear new password text
				if (newPwd) {
					newInfo = {
						task: action, 
						Pwd: newPwd
					};
				}
				else {
					alert('Cancelled. Password NOT reset');
					action = '';
				}
				break;
			//==================================================================
			case 'btnDisable':
				newInfo = {task: action};
				break;
			//==================================================================
			case 'btnReactivate':
				newInfo = {task: action};
				break;
			//==================================================================
			default:
			  // do nothing
		}
		newInfo = JSON.stringify(newInfo);
		$.ajax({
			url: 'ii-Admin/adminAccessUpdate.php',
			type: 'POST',
			data: {
				rowId: rowId,
				info: newInfo
			},
			success: function(data) {
				$('#testing').html('');
				if (action) {
					$('#testing').html(data);
				}
				var activeBtnId = $('button.active').attr('id');
				$('#'+activeBtnId).click();
			}
		}); 
	}
}

function decodeInput(e) {
	alert("Here");
}