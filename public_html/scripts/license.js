/* 
 * scripts to do with licensing
 * @count(main_str, sub_str) counts and returns number of ocurrences of substring in main string.
 *		4608030A-D65C-541D-6FD8-200D-FA5D-D551-A3FF-A100-14.1.1492
 */

var serialNumber = {
	serverString: "", 
	modeCode: "",
	appCode: "",
	version: "",
	build: "",
	unlockDevice: "",
	registeredTo: "",
	decode(serialNo) {
		var lastDot = serialNo.lastIndexOf('.') + 1;
		this.serverString = serialNo.substr(0,8);
		this.modeCode = serialNo.substr(39,4);
		this.appCode = serialNo.substr(44,4);
		this.version = serialNo.substr(49, lastDot - 50);
		this.build = serialNo.substr(lastDot);
	}
};

//==============================================================================
	
function getDetails(searchString) {
	$.ajax({
		url: 'ii-Licensing/licUtils.php',
		type: 'GET',
		data: {
			searchString: searchString
		},
		success: function(data) {
			$('#divResult').html(data);
		}
	})
};

function getModeList() {

};

function getAppList() {

};

function decodeSearchString(searchString) {
//	let isSerialNo = false,
//		isDongleNo = false;

	if (count(searchString, '-') === 9) {
		console.log("Serial Number");
//		isSerialNo = true;
		searchType = 'Serial'
		serialNumber.decode(searchString);
		console.log("get info via serial number");
		console.log(serialNumber);
		getDetails(searchString);
	}
	else if (searchString.substr(0,2).toLowerCase() === 'ii') {
		console.log("get info via dongle number");
//		isDongleNo = true;
		searchType = 'Dongle';
	}
	else {
		console.log("search Users and Companies");
		info = getDetails(searchString);
	}
//	let companyInfo = getCompanyInfo(searchString);
//	getAppInfo();
//	getUserInfo();
//	getPastUnlocks();
	
}

function count(main_str, sub_str) {
    main_str += '';
    sub_str += '';
    if (sub_str.length <= 0) {
        return main_str.length + 1;
    }
	subStr = sub_str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
	return (main_str.match(new RegExp(subStr, 'gi')) || []).length;
 }


