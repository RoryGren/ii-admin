<?php
	if (!$_SESSION) {session_start();}
//	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once '../config.php';
			include_once ROOT . "/includes/adminConfig.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User    = $_SESSION['UID'];
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			include_once CLASSES  . 'classWebContactDisplay.php';
			$Display = new classWebContactDisplay();
			$rowId   = filter_input(INPUT_POST, 'rowId', FILTER_VALIDATE_INT);
			$checked = filter_input(INPUT_POST, 'isChecked');
			if (!$Display->webContactUpdate($rowId, $checked)) {
				echo "<br>There was an error updating the database.<br>";
			}
	}
	else {
	header('location: index.php');
	}
?>
