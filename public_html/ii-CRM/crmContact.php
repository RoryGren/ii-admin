<?php
	if (!$_SESSION) {session_start();}
//	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once '../config.php';
			include_once ROOT . "/includes/adminConfig.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . 'classContactDisplay.php';
			$Display = new classContactDisplay($User[0]);
			$ContactList = $Display->getContactList();
//			sort($ContactList);
?>
<script>
	sessionStorage.setItem('sender','Contact');
	sessionStorage.setItem('senderParent','');
	var form_modified = false;
//alert(sessionStorage.getItem('module'));

$(document).ready(function(){
	//	Setup Filtering
    $('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
		$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
		
    });

	$(document).on('change', 'select', function() {
		form_modified = true;
	}); // take care of select tags

	$(document).on('change keypress', 'input', function() {
		form_modified = true;
	});

	$('.modal .btn').click(function(){
		doModalClick(this.id);
	});

//	===== setup listeners for tr clicks =====
	$('#tableList>tbody>tr').on('click', function() {
		alert(this.id);
		
		showDetail(this.id);
	})
});
</script>
<div id="mainList" class="container-fluid">
	<div class="row">
		<div class="col col-sm-4" style="padding: 0px 5px; margin-top: -28px; ">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<h4>Contact List   <span id="RowCount" class="small"></span></h4>
					<div>
						<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
						<button class="btn btn-default btn-xs btn-danger" onclick="addNew();">New Contact</button>
					</div>

				</div>
				<div id="userList" class="container-fluid">
<?php
	$Display->tableHeader($ContactList);
	$Display->tableBody($ContactList);
?>
				</div>
			</div>
		</div>
		<!--<div class="col col-sm-1"></div>-->
		<div class="col col-sm-8" id="divDetails">
			
		</div>
	</div>
</div>

<!-- Modal -->
<!--<div id="modalDetail" class="modal fade" role="dialog">
	<div class="modal-dialog">
		 Modal content
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title">Course Detail</h4>
			</div>
			<p class="btn-danger text-center" style="margin-bottom: 0px;">Changes made will be implemented immediately.</p>
			<div class="modal-body" id="modal-body">
				
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-success" id="btnSave">Save</button>
				<button type="button" class="btn btn-default btn-danger" id="btnDelete">Delete Record</button>
				<button type="button" class="btn btn-default" id="btnCancel">Cancel</button>
				<button type="button" class="btn btn-default">Close</button>
			</div>
		</div>

	</div>
</div>-->

<?php 
	}
	else {
	header('location: index.php');
	}
?>
