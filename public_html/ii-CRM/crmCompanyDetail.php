<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			$rowId = filter_input(INPUT_GET, 'rowId');
			include_once CLASSES . "classCompanyDisplay.php";
			$Display = new classCompanyDisplay($User[0]);
			$CompanyDetail = $Display->companyDetail($rowId);
?>

<!--<h3>Company Detail</h3>-->
<ul class="nav nav-tabs padded-top">
  <li class="active"><a data-toggle="tab" href="#home">Contact</a></li>
  <li><a data-toggle="tab" href="#users">Users</a></li>
  <li><a data-toggle="tab" href="#licensing">Licensing</a></li>
</ul>

<div class="tab-content padded-top">
<!--=========================================================================-->
<!--===== Company and Branch Details ========================================-->
<!--=========================================================================-->
	<div id="home" class="tab-pane fade in active">
		<form class="form-horizontal" id="frmCompany">
			<div class="form-group row">
				<div class="col-sm-4">
					<input type="text" class="form-control" id="Coy_Code" placeholder="Abbreviation / Code" value="<?php echo $CompanyDetail['Code']; ?>">
				</div>
				<div class="col-sm-8">
					<input type="hidden" id="Coy_Id" value="<?php echo $rowId; ?>">
					<input type="text" class="form-control" id="Coy_Desc" placeholder="Company Name" value="<?php echo $CompanyDetail['Company']; ?>">
				</div>
			</div>
		</form>
		<hr>
<!--=========================================================================-->
	
		<?php 
			$Display->branchDetail($rowId); 
		?>
  </div>
<!--=========================================================================-->
  <div id="users" class="tab-pane fade">
    <h3>Users</h3>
    <p>Some content in menu 1.</p>
  </div>
<!--=========================================================================-->
  <div id="licensing" class="tab-pane fade">
    <h3>Licensing</h3>
    <p>Some content in menu 2.</p>
  </div>
<!--=========================================================================-->
</div>
<?php 
//			TODO =====> listener for field change in branch details
		}
else {
	header('location: index.php');
	}
?>
<script>
	$(document).ready(function() {
		sessionStorage.setItem('changedFields', '');
		sessionStorage.setItem('selectedCompany', $('#Coy_Id').val());
	});
	var inp      = '', // input id
		oldVal   = ''; // original value
//		saveKeys = [13, 9];
		saveKeys = [13]; // Press Enter to save changes.
	
//	$('.tab-content .form-control').focusin(function(e) {
//		inp = e.target.id;
//		oldVal = e.target.value;
//		
//	});

	$('.tab-content .form-control').keyup(function(e) {
		// Info needed: id, oldVal, newVal, isAttrib or isBranchCode
		console.log("input: "+inp);
		if (saveKeys.includes(e.which)) {
			if (!inp) { // Empty field - need to add record
				console.log("New attrib record...");
			}
			else { // Existing record. Update when changed
//				console.log("Active Element: "+inp+" :: "+oldVal);
				console.log("id: "+e.target.id);
				console.log("defaultValue: "+e.target.defaultValue);
				console.log("newValue: "+e.target.value);
				console.log("dataset: ");
				console.log(e.target.dataset);
//				console.log($("#"+inp).val());
			}
//			TODO =====> add update & insert :: need to distinguish between masters and attributes via data tags
		}
//		alert(this.id+" :: "+e.keyCode);
	});
//	$('input.form-control.branchData').on('change', function(e) {
//		alert(this.id+" has changed ");
//	});

</script>