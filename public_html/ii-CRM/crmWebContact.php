<?php
if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . 'classWebContactDisplay.php';
			$Display = new classWebContactDisplay();
?>
<script>
	$(document).ready(function() {
		$("input[type='checkbox']").change(function() {
			updateWebContacts(this.id);
		});
	})
</script>
<div class="padded-top"></div>
<div class="padded-top"></div>
<h4>Contacts via Website</h4>
<?php
	$Display->webContacts();
?>
<?php 

	}
	else {
	header('location: index.php');
	}
?>
