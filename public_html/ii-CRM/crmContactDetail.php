<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			$rowId = filter_input(INPUT_GET, 'rowId');
			include_once CLASSES . "classContactDisplay.php";
			$Display = new classContactDisplay($User[0]);
			$ContactDetail = $Display->userDetail($rowId);
?>

<!--<h3>Company Detail</h3>-->
<ul class="nav nav-tabs padded-top">
  <li class="active"><a data-toggle="tab" href="#home">Company</a></li>
  <li><a data-toggle="tab" href="#users">Users</a></li>
  <li><a data-toggle="tab" href="#licensing">Licensing</a></li>
</ul>

<div class="tab-content padded-top">
	<div id="home" class="tab-pane fade in active">
<!--    <h3>HOME</h3>
    <p>Company detail</p>-->
		<form class="form-horizontal">
			<div class="form-group">
				<div class="col-sm-12">
					<input type="hidden" id="Coy_Id" value="<?php echo $rowId; ?>">
					<input type="text" class="form-control" id="Coy_Desc" placeholder="Company Name" value="<?php echo $CompanyDetail['Description']; ?>">
				</div>
			</div>
		</form>
	
	<hr>
	<h3>Branches</h3>
  </div>
  <div id="users" class="tab-pane fade">
    <h3>Users</h3>
    <p>Some content in menu 1.</p>
  </div>
  <div id="licensing" class="tab-pane fade">
    <h3>Licensing</h3>
    <p>Some content in menu 2.</p>
  </div>
</div><?php 
			
	}
	else {
	header('location: index.php');
	}
?>
<script>
	
</script>