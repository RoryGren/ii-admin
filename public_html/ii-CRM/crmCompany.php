<?php
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
		
			include_once CLASSES . "classCompanyDisplay.php";
			$Display = new classCompanyDisplay($User[0]);
			$CompanyList = $Display->getCompanyList();
?>
<div id="mainList" class="container-fluid">
	<div class="row">
		<div class="col col-sm-3" style="padding: 0px 5px; margin-top: -28px; ">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<h4>Company List   <span id="RowCount" class="small"></span></h4>
					<div>
						<button class="btn btn-default btn-xs btn-filter hidden"><span class="glyphicon glyphicon-filter"></span> Filter</button>
						<button class="btn btn-default btn-xs btn-danger" onclick="addNew('Company');">New Company</button>
					</div>

				</div>
				<div class="pre-scrollable" style="min-height: 80vh;">
<?php
	$Display->companyList();
?>
				</div>
			</div>
		</div>
		<div class="col col-sm-9" id="divDetails">
			
		</div>
	</div>
</div>
<script>
sessionStorage.setItem('sender','Company');

$(document).ready(function(){
//	===== setup menu display =====
	$('#navCRM').siblings('li').removeClass('active');
	$('#navCRM').addClass('active');
	
//	===== setup filters =====
	$('.filterable .btn-filter').click(function(){
        var $panel = $(this).parents('.filterable'),
        $filters = $panel.find('.filters input'),
        $tbody = $panel.find('.table tbody');
        if ($filters.prop('disabled') == true) {
            $filters.prop('disabled', false);
            $filters.first().focus();
        } else {
            $filters.val('').prop('disabled', true);
            $tbody.find('.no-result').remove();
            $tbody.find('tr').show();
        }
    });
	
//	$('.filters th:first input').val('Search:');
	var companyFilter = $('.filters th:last input');
	companyFilter.prop('disabled', false);
	companyFilter.focus();

    $('.filterable .filters input').keyup(function(e){
        /* Ignore tab key */
        var code = e.keyCode || e.which;
        if (code == '9') return;
        /* Useful DOM data and selectors */
        var $input = $(this),
        inputContent = $input.val().toLowerCase(),
        $panel = $input.parents('.filterable'),
        column = $panel.find('.filters th').index($input.parents('th')),
        $table = $panel.find('.table'),
        $rows = $table.find('tbody tr');
        /* Dirtiest filter function ever ;) */
        var $filteredRows = $rows.filter(function(){
            var value = $(this).find('td').eq(column).text().toLowerCase();
            return value.indexOf(inputContent) === -1;
        });
        /* Clean previous no-result if exist */
        $table.find('tbody .no-result').remove();
        /* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
        $rows.show();
        $filteredRows.hide();
		$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
        /* Prepend no-result row if all rows are filtered */
        if ($filteredRows.length === $rows.length) {
            $table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
        }
		
    });

//	===== form behaviour =====
	$(document).on('change', 'select', function() {
		form_modified = true;
	}); // take care of select tags

	$(document).on('change keypress', 'input', function() {
		form_modified = true;
	});

	$('.modal .btn').click(function(){
		doModalClick(this.id);
	});

//	===== setup listeners for tr clicks =====
	$('#tableList>tbody>tr').on('click', function(e) {
//		console.log("Row Click - Branch_Id: "+$(this).attr('data-rowid'));
		showCompanyDetail($(this).attr('data-rowid'));
		$('#tableList>tbody>tr.active').removeClass('active');
		$(this).addClass('active');
	})
});
</script>
<?php 
	}
	else {
	header('location: index.php');
	}
?>
