<?php
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . "classLogin.php";
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
?>
<!DOCTYPE html>
<html>
    <head>
	<?php 
		include HEAD; 
	?>
	</head>
	<body>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<h4>Bulk Emails</h4>
<?php

class classDB extends MYSQLi { 

	private $DB_NAME = 'c350324_admin';
	private $DB_HOST = 'localhost';
	private $DB_USER = 'c350324_webusr';
	private $DB_PASS = 'iiWebusr299';

	//Establish Connection 
	function __construct() { 
		/*
		 * Initialises mysqli database connection
		 */
		$this->connect($this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME); 
//		echo "$this->DB_HOST, $this->DB_USER, $this->DB_PASS, $this->DB_NAME<br>";
	} 

	//Select Query 
	private function fetchData() { 
//		$SQL = "SELECT * FROM `ClientContactEmail`";
		$SQL = "SELECT * FROM `ClientContactEmail` ORDER BY `Who Requested` DESC LIMIT 8";
//		echo $SQL . "<br>";
		return $this->query($SQL);
	} 
	
	public function getData() {
		echo "getting data<br>";
		$result = $this->fetchData();
		while ($data = mysqli_fetch_assoc($result)) {
			$Client[] = $data;
		}
		return $Client;
	}

}
	$DB = new classDB();
	$AllClients = $DB->getData();
	echo "Preparing message.<br>";
//	print_r($AllClients);
	
	$header = "<html><head><style>body{color:#333;font-family:Arial;}ul li {line-height:180%;}.top{background-color: #243B72; min-height: 90px;}</style></head><body>";
	$footer = "</body></html>";
	$bodyTop = "<div class='top'>
			<a href='http://www.reticmaster.com/' target='_blank'>
				<img src='http://www.reticmaster.com/wp/wp-content/uploads/2013/12/logo-on-blue.png' alt='Inspired Interfaces'>
			</a>
		</div>
		<div>
			<div style='padding-left: 20px; border: 3px double #000000;margin: 3px 0px;'>
				<p>";
	$bodyRest = "</p>
				<p>This is just a quick note to thank you for your support during 2017 and to wish you a blessed Christmas and look forward to a prosperous new year with you in 2018!</p>
				<p>Please note that our offices will be closed over the Christmas period from 15 December 2017 and re-opening on 8 January 2018.</p>
				<p>We will, however, be monitoring our emails during this period and will still be available for email support and to issue unlock codes where required.</p>
				<div style='background-color: #F3AA30; padding: 3px 10px; margin: 10px 15px 10px 0px;'>
					<p>Please take note that new builds of both ReticMaster and PowerOffice have been released and are available for download from our website at 
						<span><a href='http://www.reticmaster.com' target='_blank'>www.reticmaster.com</a></span>, and users are encouraged to update.</p>
					<p>These updates address minor bugs and add new features such as compatibility with the latest MS Office files for importing and exporting.</p>
					<p>Updating to this build of PowerOffice will require a quick database update to <b>Patch E(195R14)</b> using the PO Scripter tool.</p>
					<p>Download the following documents to guide you throught the update process:</p>
					<ul>
						<li><a href='http://reticmaster.com/Files/PowerOffice/Docs/12b.InstallingPatchforPowerOffice14Client.pdf' target='_blank'>Updating the PowerOffice Client</a></li>
						<li><a href='http://reticmaster.com/Files/PowerOffice/Docs/12d.InstallingPatchforPowerOfficeDataBaseServer.pdf' target='_blank'>Updating the PowerOffice Database</a></li>
						<li><a href='http://reticmaster.com/Files/PowerOffice/Docs/12a.InstallingPatchforReticMaster14.pdf' target='_blank'>Updating ReticMaster</a></li>
					</ul>
				</div>
				<p>From the Inspired Interfaces Team.</p>
			</div>
		</div>
		<div class='top'>
			<a href='#' alt='#'>
				<img src='http://www.reticmaster.com/Files/Misletoe.png' alt='Mistletoe'>
			</a>
		</div>";
		$Person  = "Joseph McSoap";
		$Counter = 0;

	foreach ($AllClients as $Key => $data) {
		
		$Person = $data['Who Requested'];
		$Salutation = "Dear " . $Person;
//		echo $Salutation . "<br>";

		$to   = $data['email'];
//		$to   = "rory@websurfer.co.za";
		$cc   = "";
		$from = "support@reticmaster.com";
		$bcc  = "blindcopy@reticmaster.com";

		$subject  = "PowerOffice and ReticMaster Updates";
		
		// Always set content-type when sending HTML email
		$headers  = '';
		$headers .= "From: $from" . "\r\n";
		$headers .= "Reply-To: $from" . "\r\n";
		$headers .= "CC: $cc" . "\r\n";
		$headers .= "BCC: $bcc" . "\r\n";

		$headers .= "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=ISO-8859-1" . "\r\n";

	$html = $header . $bodyTop . $Salutation . $bodyRest . $footer;
	$html = wordwrap($html, 50); 

echo $html;
//		$send = mail($to, $subject, $html, $headers);
		if ($send) {
//			echo "<p>An email message has been sent to " . $data['Who Requested'] . " at " . $to . "</p>";
			$Counter ++;
//			echo $pageMessage;
		}
		else {
//			echo "Error sending mail<br>Please contact support.";
		}
//		echo "<p>An email message has been sent to " . $data['Who Requested'] . " at " . $to . "</p>";
//		$Counter ++;
	}
	
	echo $Counter . " messages sent.<br>";
		echo $html;

//print_r($AllClients);

?>
		<script>
			$(document).ready(function() {
					$('#navAdmin').siblings('li').removeClass('active');
					$('#navAdmin').addClass('active');
					sessionStorage.setItem('module', 'admin');
				});
		</script>
    </body>
</html>

<?php 
	}
	else {
		header('location: index.php');
	}
?>