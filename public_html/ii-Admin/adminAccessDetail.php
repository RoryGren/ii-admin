<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			$rowId = filter_input(INPUT_GET, 'rowId');
			include_once CLASSES . "classAdminDisplay.php";
			$Display = new classAdminDisplay($User);
			$UserDetails = $Display->userDetails($rowId);
			$statusButton  = "<button type='button' class='btn btn-info btnAccessControl' id='btnDisable'>Deactivate</button>";
			if ($UserDetails['Per_Level'] == 0) {
				$statusButton  = "<button type='button' class='btn btn-info btnAccessControl' id='btnReactivate'>Re-activate</button>";
			}
?>
<h4>User Access: <?php echo $UserDetails['Per_Name']; ?></h4>
<form id="frmUserAccess">
	<div class="form-group">
		<label for="Name">Name:</label>
		<input type="text" class="form-control" id="Name" value="<?php echo $UserDetails['Per_Name'] ?>">
	</div>
	<div class="form-group">
		<label for="Desc">Login Name:</label>
		<input type="text" class="form-control" id="Desc" value="<?php echo $UserDetails['Per_Desc'] ?>">
	</div>
	<div class="form-group">
		<label for="Short">Abbreviation:</label>
		<input type="text" class="form-control" id="Short" value="<?php echo $UserDetails['Per_Short'] ?>">
	</div>
	<div class="form-group" id="divLevel">
		<label for="Level">Level:</label>
		<input type="text" class="form-control" id="Level" disabled value="<?php echo $UserDetails['Per_Level'] ?>" title="Just uses '1111' for active and '0' for inactive.">
		<input type="hidden" class="form-control" id="rowId" disabled value="<?php echo $rowId ?>">
	</div>
	<?php echo $statusButton; ?>
	<button type="button" class="btn btn-danger btnAccessControl" id="btnReset">Reset Password</button>
	<button type="button" class="btn btn-success btnAccessControl" id="btnSave">Save Changes</button>
	
</form>
<div class="padded-top"></div>
<div id="testing"></div>
<script>
	$(document).ready(function() {
		if ($('#rowId').val() === 'new') {
			$('#btnReactivate').addClass('hidden'); //attr('display', 'disabled');
			$('#btnReset').addClass('hidden');
			$('#divLevel').addClass('hidden');
			$('#Level').val('1111');
			$('#btnSave').text('Save');
			$('#btnSave').attr('disabled', 'disabled');
		}
	});
	$('.form-group input:text').on('keyup', function() {
		if ($('#Name').val() !== '' && $('#Desc').val() !== '' && $('#Short').val() !== '') {
			$('#btnSave').removeAttr('disabled');
		}
		else {
			$('#btnSave').attr('disabled', 'disabled');
		}
	});
	$('button').on('click', function() {
		doAdminAccessUpdate(this.id);
	});
</script>

<?php

	}
	else {
		header('location: index.php');
	}
?>