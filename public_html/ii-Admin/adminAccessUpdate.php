<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			$rowId = filter_input(INPUT_POST, 'rowId');
			$info  = $_POST['info'];
			include_once CLASSES . "classAdminDisplay.php";
			$Display = new classAdminDisplay($User);
			$Display->updateUserData($rowId, $info);
	
?>




<?php

	}
	else {
		header('location: index.php');
	}
?>

