<?php
	if (!$_SESSION) {session_start();}
//	$_SESSION: Array ( 
//	[LoggedIn] => YouAreLoggedInToTheAdminModuleNow 
//	[LogInStart] => 2018-06-05 14:01:22 
//	[LoggedInToken] => New 
//	[UID] => Array ( 
//		[0] => 1 
//		[Per_Id] => 1 
//		[1] => Rory 
//		[Per_Name] => Rory ) )
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
			include_once "../config.php";
			include_once ROOT     . "/includes/adminConfig.php";
			include_once WEB_ROOT . '/classes/classLogin.php';
			include_once CLASSES . "classSupport.php";
			$Support = new classSupport();
?>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
	<h3>Admin Support</h3>
	<div class="container-fluid">
		<div class="row">
			
			<!--<div class="col col-lg-1"></div>-->
			<div class="col col-lg-12" id="working">
				<div id="divLeft" style="border: 1px solid red; float: left;">
					<table style='padding:5px;'>
					<tr>
						<td>
							Logged In:
						</td>
						<td>
							<?php
							echo $_SESSION['UserName'];
							?>
						</td>
					</tr>
					<tr>
						<td>
							Type:
						</td>
						<td>
							<select id="selActivity" onchange="if(this.selectedIndex>0){document.getElementById('txtStartTime').value=doTime();}">
								<option value="999999">-- Select --</option>
								<?php echo $Act; ?>
							</select>
							<input type="text" id="txtStartTime" class="noBorder" size="40">
						</td>
					</tr>
					<tr>
						<td>
							User:<input type="hidden" id="txtUserId" value="ZZ">
						</td>
						<td>
							<input type="text" id="txtSearch" OnKeyUp="newFile(this.value, '0', 'divList', 'user_list');">&nbsp;<input type="text" id="txtSurn">
							<!--<div id="divList1"></div>-->
						</td>
					</tr>
					<tr>
						<td>
							Client:
						</td>
						<td>
							<select id="selClient" onclick="document.getElementById('divList').innerHTML='';">
								<option value="999999">-- Select --</option>
								<option value="999990">None</option>
								<?php echo $Cust; ?>
							</select>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td>
							Application:
						</td>
						<td>
							<input type="checkbox" id="cbRM">&nbsp;ReticMaster&nbsp;<input type="checkbox" id="cbPM">&nbsp;PowaMaster&nbsp;<input type="checkbox" id="cbPO">&nbsp;PowerOffice
						</td>
					</tr>
					<tr valign="top">
						<td>
							Comment:
						</td>
						<td>
							<textarea cols="50" rows="5" id="txtActDesc"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
						<td valign="top">
							<input type="button" id="btnActivity" value="Completed" onclick="newFile(document.getElementById('selActivity').value+'&Client='+document.getElementById('selClient').value+'&ClientId='+document.getElementById('txtUserId').value+'&NewClient='+document.getElementById('txtSearch').value+'&Surn='+document.getElementById('txtSurn').value+'&Comment='+document.getElementById('txtActDesc').value+'&RM='+document.getElementById('cbRM').checked+'&PM='+document.getElementById('cbPM').checked+'&PO='+document.getElementById('cbPO').checked, doTime()+'&StartTime='+document.getElementById('txtStartTime').value, 'divTimes', 'admin_log_list'); doEntry();">
						</td>
					</tr>
					</table>
				</div>
				<div id="divList" style="width: 400px; float: left; border: 1px solid yellow;"></div>

				<div id="divRight" style="width:800px"></div>
					<!--<hr>-->
				<div id="divBottom">
					<p align="left"><input type="button" id="btnRefresh" value="Refresh" onclick="newFile('zzzzzz', 'Refresh', 'divTimes', 'admin_log_list')"></p>
				</div>
				<div id="divTimes" align="left">

				</div>
			</div>
			<!--<div class="col col-lg-1"></div>-->
		
		</div> <!--End Row-->
	</div> <!--End Container-fluid-->
	<script>
		$(document).ready(function() {
				$('#navHome').siblings('li').removeClass('active');
				$('#navHome').addClass('active');
				sessionStorage.setItem('module', 'home');
			});
	</script>
<?php 
	}
	else {
	header('location: index.php');
	}
