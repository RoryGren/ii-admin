<?php
if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES  . "classLogin.php";
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			$User = $_SESSION['UID'];
			include_once CLASSES . 'classAdminDisplay.php';
			$Display = new classAdminDisplay($User);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col col-sm-6">
			<h4>User Access</h4>
			<?php $Display->userList(); ?>
		</div>
		<div class="col col-sm-1"></div>
		<div class="col col-sm-5" id="userDetail">
			<!--<h4>User</h4>-->
		</div>
	</div>
</div>
<script src="scripts/admin.min.js" type="text/javascript"></script>
<script>
	sessionStorage.setItem('sender', 'adminAccess');
	$('#tableList>tbody>tr').on('click', function() {
		var rowId = this.id;
		$('#'+rowId).siblings('tr').removeClass('active');
		$('#'+rowId).addClass('active');
		showAccessDetail(rowId);
	})
	$('#btnNew').on('click', function() {
		$('#tableList>tbody>tr').removeClass('active');
		showAccessDetail('new');
	})
</script>
	
<?php 
	}
	else {
	header('location: index.php');
	}
?>

