<?php
	if (!$_SESSION) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
			include_once "config.php";
			include_once ROOT     . "/includes/adminConfig.php";
			include_once WEB_ROOT . '/classes/classCompany.php';
?>
<!DOCTYPE html>
<?php 
	include WEB_ROOT . '/includes/head.php'; 
?>
<body>
	<?php
		include WEB_ROOT . '/includes/navBarAdmin.php';
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col col-md-2 menu-left">
				<h4 class="text-center">Company</h4>
				<button id="coyMaster" class="btn btn-dark btn-block active" title="Modify existing Course data">Master</button>
				<button id="coyAdd"  class="btn btn-dark btn-block"  title="Modify existing Student data">Add</button>
				<button id="coySomething" class="btn btn-dark btn-block" title="Manual capture">So Something</button>
			</div>
			<div class="col col-md-4" id="working">
				<?php include 'ii-CRM/companyList.php' ?>
			</div>
			<div class="col col-md-5" id="detail"></div>
			<div class="col col-md-1"></div>
		</div>
	</div>
<?php 
	}
	else {
	header('location: index.php');
	}
?>