<?php
	if (!isset($_SESSION)) {session_start();}
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . 'classLogin.php';
			$User = $_SESSION['UID'];
			$_SESSION['TimeOut'] = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
?>
<!DOCTYPE html>
<html>
    <head>
<?php 
	include HEAD; 
?>
	</head>
	<body>
		<script>
			$(document).ready(function() {
				// ===== Set Navbar display =====
				$('#navTraining').siblings('li').removeClass('active');
				$('#navTraining').addClass('active');
				// ===== Set default values =====
				sessionStorage.setItem('module', 'Training');
				$(document).attr('title', 'II Admin | Training');
				// ===== .leftNav button click listener =====
				$('.leftNav>button').on('click', function(event) {
					showFile(event);
				});
				// ===== .modal button click listener =====
				$('.modal .btn').click(function(){
					doModalClick(this.id);
				});
//				$('.modal-body').on('change', 'select', function() {
//					var filterVal = $('#'+this.id).val();
//					if (this.id='App_Id') {
//						$("#Course_Id").children('option').hide().not('option[data-appId='+filterVal+']');
//						$('#Course_Id').find('option').data('data-appId', $('#App_Id:selected').val()).show();
//						$("#Course_Id").children("option[value^=" + $(this).val() + "]").show()
//						alert('data value: '+App_Id);
//					};
//				});
			});
		</script>
		<?php
			include WEB_ROOT . '/includes/navBarAdmin.php';
		?>
		<div class="container-fluid">
			<div class="row padded-top">
				<div class="col col-md-2 leftNav">
					<h4 class="text-center">Training</h4>
					<button id="trainCourse"  class="btn btn-dark btn-block active" title="Modify existing Course data">Courses</button>
					<button id="trainStudent" class="btn btn-dark btn-block"		title="Modify existing Student data">Students</button>
					<button id="trainMasters" class="btn btn-dark btn-block"		title="Maintain Master Lists">Masters</button>
					<button id="trainReport"  class="btn btn-dark btn-block"		title="Reporting">Reporting</button>
				</div>
				<div class="col col-md-9" id="working">
					<?php include 'ii-Training/trainCourse.php' ?>
				</div>
				<div class="col col-md-1"></div>
			</div>
		</div>
    </body>
</html>
<?php 
		include WEB_INC . 'modal.php';
	}
	else {
	header('location: index.php');
	}
?>
