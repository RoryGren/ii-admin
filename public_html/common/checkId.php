<?php
	if (!$_SESSION) {session_start();}
//	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "../config.php";
			include_once ROOT_INC . "adminConfig.php";
//			include_once CLASSES  . "classLogin.php";
			$User = $_SESSION['UID'];
			$studentId = filter_input(INPUT_GET, 'User_Id_No', FILTER_SANITIZE_NUMBER_INT);
			$selectedCourseId = filter_input(INPUT_GET, 'Selected_Course_Id', FILTER_SANITIZE_NUMBER_INT);
			include CLASSES  . "/classStudentDisplay.php";
			$Display   = new classStudentDisplay($User[0]);
			$userRowId = $Display->returnUserIdNo($studentId);
			$enrolled  = $Display->userEnrolledInCourse($userRowId, $selectedCourseId);
			if ($enrolled) {
				echo trim($userRowId);
			}
			else {
				echo "-Enrolled";
			}
			//TODO !!!! Where does double enter come from in rowId???
			// workaround by checking for "new" at beginning of $studentId
?>


<?php
		}
		else {
			header('location: index.php');
		}
?>
