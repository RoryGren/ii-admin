        <meta charset="UTF-8">
		
		<!--Style Sheets-->
		<!--===========================================================================-->
		<!-- For Online use uncomment the following two lines and comment the next two --> 
		<!--===========================================================================-->
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" >
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--=========================================================================-->
		<!--<link href="style/bootstrap.css" rel="stylesheet" type="text/css"/>-->
		<!--<link href="../style/font-awesome.min.css" rel="stylesheet" type="text/css"/>-->
		<!--=========================================================================-->
		<!--Custom Style Sheets-->
		<link href="style/nav.min.css?d=<?php echo date('Ymd:h:i:s'); ?>" rel="stylesheet" type="text/css"/>
		<link href="style/ii.min.css?d=<?php echo date('Ymd:h:i:s'); ?>" rel="stylesheet" type="text/css"/>
		<!--=========================================================================-->
		<!--Javascript & JQuery-->
		<!--=========================================================================-->
		<!-- Online Use Only -->
		<!--<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>-->
		<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
		<!--=========================================================================-->
		<!--Local Use Only-->
		<script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<!--=========================================================================-->
		<!--Custom Javascripts-->
		<!--<script src="scripts/scripts.min.js?d=<?php echo date('Ymd:h:i:s'); ?>" type="text/javascript"></script>-->
		<script src="scripts/scripts.js" type="text/javascript"></script>
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
        <title>II Admin</title>
