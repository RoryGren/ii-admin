<?php
//	$_SESSION: Array ( 
//	[LoggedIn] => YouAreLoggedInToTheAdminModuleNow 
//	[LogInStart] => 2018-06-05 14:01:22 
//	[LoggedInToken] => New 
//	[UID] => Array ( 
//		[0] => 1 
//		[Per_Id] => 1 
//		[1] => Rory 
//		[Per_Name] => Rory ) )
?>
		<nav class="navbar navbar-inverse" role="navigation" id="topMenu">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header redBorder">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="adminDash.php">ii-Admin</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left redBorder">
						<li id="navHome"><a href="CRM.php">CRM Home</a></li>
						<li id="navCoy" class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Company <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="company.php"><i class="fa fa-building-o"></i> Companies</a></li>
								<li><a href="branch.php"><i class="fa fa-tree"></i> Branches</a></li>
							</ul>
						</li>
						<li id="navLicenses" class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Licensing <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="licApps.php"><i class="fa fa-map-o"></i> Applications</a></li>
								<li><a href="licCompany.php"> Companies</a></li>
								<li><a href="licUsers.php"><i class="fa fa-question"></i><i class="fa fa-question"></i> Users</a></li>
							</ul>
						</li>
						<li id="navTraining" class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Training <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="training.php"><i class="fa fa-group"></i> Face-to-Face</a></li>
								<li><a href="online.php"><i class="fa fa-desktop"></i> Online</a></li>
								<li><a href="report.php"><i class="fa fa-question"></i><i class="fa fa-question"></i> Reporting</a></li>
							</ul>
						</li>
						<li id="navTraining" class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Stuff <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href=".php"><i class="fa fa-group"></i> Some Stuff</a></li>
								<li><a href="whySoftware.php"><i class="fa fa-desktop"></i> More Stuff</a></li>
								<li><a href="brochures.php"><i class="fa fa-question"></i><i class="fa fa-question"></i> Other Stuff</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right redBorder">
						<li id="menu-signed-in">
							<a class="navbar-brand" href="#">
								<span class="glyphicon glyphicon-user"></span>&nbsp;
								<?php echo $User[1]; ?>
							</a>
						</li>
						<li id="menu-Logout" onclick="">
							<a class="navbar-brand" href="logout.php" title="Logout"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp; </a></li>
					</ul>
				</div>
			</div>
		</nav>
