<?php
// TODO =====> Report-a-Bug |Front-end and Back-end
// TODO =====> Request-a-feature | Front-end and Back-end
// TODO =====> Requests and Bug-fix progress
// TODO =====> Licensing Search by Serial No
// TODO =====> Licensing Search by Company
// TODO =====> Licensing Search by Region
// TODO =====> Bulk email sending.... by app, by region, selected...
	$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			$User = $_SESSION['UID'];
//			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			include_once CLASSES . 'classLogin.php';

?>
		<header class="navbar navbar-inverse" role="banner" id="topMenu">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="adminDash.php">ii-Admin</a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<!--<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation" id="navbarNav">-->
			<nav class="collapse navbar-collapse container-fluid" role="navigation" id="bs-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li id="navHome"><a href="adminDash.php">Home</a></li>
					<!--<li id="navCRM"><a href="CRM.php">CRM</a></li>-->
					<li id="navCRM" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">CRM <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li title="Company"><a href="CRM.php"><i class="fa fa-user-circle"></i> CRM</a></li>
							<li title="Users"><a href="support.php" target="_blank"><i class="fa fa-folder-o"></i> Support</a></li>
							<li title="Licenses"><a href="licensing.php"><i class="fa fa-lock"></i> Licensing</a></li>
							<!--<li title="Web"><a href="ii-CRM/crmWebContact.php"><i class="fa fa-handshake-o"></i> Web Contacts</a></li>-->
						</ul>
					</li>
					<li id="navProducts" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li title="Undocumented Features / UFO"><a href="productKB.php"><i class="fa fa-eye"></i><i class="fa fa-question"></i> UDF Reporting</a></li>
							<li title="User Requests"><a href="productRequests.php"><i class="fa fa-plus"></i><i class="fa fa-question"></i> User Requests</a></li>
						</ul>
					</li>
					<li id="navLicenses" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Licensing <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="licApps.php"><i class="fa fa-windows"></i> Applications</a></li>
							<li><a href="licCompany.php"><i class="fa fa-building-o"></i> Company</a></li>
							<li><a href="licUsers.php"><i class="fa fa-group"></i> Users</a></li>
							<li title="Licenses"><a href="licensing.php"><i class="fa fa-lock"></i> Unlocks</a></li>
						</ul>
					</li>
					<li id="navTraining" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Training <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="training.php"><i class="fa fa-eye"></i> Face-to-Face</a></li>
							<li><a href="online.php"><i class="fa fa-desktop"></i> Online</a></li>
							<li><a href="report.php"><i class="fa fa-pencil-square-o"></i> Reporting</a></li>
						</ul>
					</li>
					<li id="navWebsite" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Website <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<!--<li><a href=".php"><i class="fa fa-handshake-o"></i> Website Contacts</a></li>-->
							<li><a href="whySoftware.php"><i class="fa fa-thumbs-o-up"></i> More Stuff</a></li>
							<li><a href="brochures.php"><i class="fa fa-trophy"></i> Other Stuff</a></li>
						</ul>
					</li>
					<li id="navAdmin" class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="adminControl.php"><i class="fa fa-lock"></i> Access Control</a></li>
							<li><a href="ii-Admin/adminSupport.php" target="_blank"><i class="fa fa-thumbs-o-up"></i> Support</a></li>
							<li><a href="bulkEmail.php"><i class="fa fa-envelope-o"></i> Bulk Email</a></li>
							<li><a href="brochures.php"><i class="fa fa-trophy"></i> Other Stuff</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li id="menu-signed-in">
						<a href="#">&nbsp;
							<!--<span class="glyphicon glyphicon-user"></span>&nbsp;-->
							<span class="fa fa-user"></span>&nbsp;
							<?php echo $User[1]; ?>
						</a>
					</li>
					<li id="menu-Logout">
						<a href="logout.php" title="Logout">
							<span class="fa fa-sign-out"></span>&nbsp;&nbsp; 
						</a>
					</li>
				</ul>
			</nav>
		</header>
<?php 
	}
	else {
	header('location: index.php');
	}
?>