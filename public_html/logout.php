<?php
	if (!$_SESSION) {session_start();}
	$TimeOut = gmdate("Y-m-d H:i:s", strtotime(" + 150 minutes"));
	if ($_SESSION['LoggedIn'] === 'YouAreLoggedInToTheAdminModuleNow' &&
		$_SESSION['LogInStart'] < $TimeOut &&
		$_SESSION['LoggedInToken'] === 'New') {
			include_once "config.php";
			include_once ROOT_INC . "adminConfig.php";
			$User = $_SESSION['UID'];
?>
<!DOCTYPE html>
<?php 
	include WEB_ROOT . '/includes/head.php'; 
?>
	<body>
	<?php
		include WEB_ROOT . '/includes/navBarAdmin.php';
		$_SESSION['LoggedIn'] = '';
		$_SESSION['LogInStart'] = '';
		$_SESSION['LoggedInToken'] = '';
		session_unset();
		session_destroy();
		echo "<meta http-equiv='refresh' content='2;url=index.php'>";
	?>
		<div class="padded-top">
			<br><br>
			<h3 class="padded-top text-center">You are being logged out...</h3>
		</div>
    </body>
</html>
<?php
	}
	else {
		header('location: index.php');
	}
?>
