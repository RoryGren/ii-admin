<?php
	ob_start();
	session_start();
//	echo "<br><Br><br><span style='color: red;'>" . $_REQUEST['Login'] . "</span><br><br><br>";
	if (count($_REQUEST) > 1 && $_REQUEST['Login'] === 'HelloAdmin') {
		include "config.php";
		include ROOT_INC . "/adminConfig.php";
		include WEB_ROOT . '/classes/classLogin.php';
//		$Today = gmdate("Y-m-d H:i:s", strtotime(" + 2 hours"));
		$Login = new classLogin();
		if ($Login->userExists($_REQUEST['userName'], $_REQUEST['pwd'])) {
//			echo "Valid Login<br>";
			// TODO =====> User Log <=====
			$_SESSION['LoggedIn'] = 'YouAreLoggedInToTheAdminModuleNow';
			$_SESSION['LogInStart'] = $Today;
			$_SESSION['LoggedInToken'] = 'New';
			$_SESSION['UID'] = $Login->user;
			$valid = WEB_ROOT . '/adminDash.php';
//			echo $valid . "<br>";
			header('location: adminDash.php');
		}
		else {
			session_destroy();
			header('location: index.php');
		}
	}
	else {
		session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		
		<!--Style Sheets-->
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<link href="style/nav.min.css?d=<?php echo date('Ymd:h:i:s'); ?>" rel="stylesheet" type="text/css"/>
		<link href="style/ii.min.css?d=<?php echo date('Ymd:h:i:s'); ?>" rel="stylesheet" type="text/css"/>
		
		<!--Javascript & JQuery-->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!--<script src="https://use.fontawesome.com/6f7c9f8c3d.js"></script>-->
		
		<script src="scripts/scripts.min.js?d=<?php echo date('Ymd:h:i:s'); ?>" type="text/javascript"></script>
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<title>II Admin Login</title>

    </head>
    <body>
		<nav class="navbar navbar-inverse" id="topMenu">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navBar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					<a class="navbar-brand" href="#">ii-Admin Login</a>
				</div>
				<div class="collapse navbar-collapse" id="navBar">
					<ul class="nav navbar-nav navbar-right">
						<li id="menu-Logout" class="navbar-right" onclick="">
							<a href="#">&nbsp;&nbsp; </a></li>
						<li id="menu-SignedIn" class="navbar-right">
							<h4 class="SignedIn" id="SignedIn"><span class="glyphicon glyphicon-user"></span>
								Not signed in
							</h4>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container-fluid padded-top">
			<div class="row padded-top">
				<div class="col col-md-4 "></div>
				<div class="col col-md-4 text-center"><h3>User Login</h3></div>
				<div class="col col-md-4 leftNav"></div>
			</div>
			<div class="row">
				<div class="col col-md-4 "></div>
				<div class="col col-md-4">
					<form class="form-vertical" method="post">
						<div class="form-group">
							<label for="userName" class="control-label text-left">Name:</label>
							<input type="text" class="form-control" id="userName" name="userName">
						</div>
						<div class="form-group">
							<label for="pwd">Password:</label>
							<input type="password" class="form-control" id="pwd" name="pwd">
						</div>
						<div class="form-group text-center">
							<button class="btn btn-block btn-dark" id="Login" name="Login" type="submit" value="HelloAdmin">Login</button>
						</div>
					</form>
				</div>
				<div class="col col-lg-4 leftNav"></div>
			</div>
		</div>
    </body>
</html>
<?php
ob_flush();
	}
?>